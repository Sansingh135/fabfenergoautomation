#Test Case: TC_R1S2EPIC002PBI038_01
#PBI: R1S2EPIC002PBI038
#User Story ID: US068
#Designed by: Niyaz Ahmed (as part of R1EPIC002PBI008)
#Last Edited by: Anusha PS
Feature: Capture New Request Details - Complete Legal Entity Category

  @To_be_automated @TC_R1S2EPIC002PBI038_01
  Scenario: Validate the 'Legal Entity Category' dropdown is filtered with relevant values (13) in Complete screen and other secondary screens when 'Client Type' is selected as 'Corporate' from the Enter entity details screen (Refer lov in the PBI)
    Given I login to Fenergo Application with "RM"
    #Select client Type as 'Corporate' in Enter entity details screen
    #When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    #Navigate to Complete screen (3 screen in the workflow)
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Corporate"
    Then I verify "Legal Entity Category" drop-down values with ClientType as "Corporate"
    #Refer PBI for LOVs
    Then I complete "Complete" screen
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "OnboardingMaker"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    Then I verify "Legal Entity Category" drop-down values with ClientType as "Corporate"
    #Refer PBI for LOVs
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    And I check that "LegalEntityCategory" is readonly
    And I Validate the Legal entity category value is defaulted from complete screen/Validate KYC and Regulatory Data