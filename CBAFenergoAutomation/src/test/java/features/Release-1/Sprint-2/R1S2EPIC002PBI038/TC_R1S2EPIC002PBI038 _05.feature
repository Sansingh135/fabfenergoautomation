#Test Case: TC_R1S2EPIC002PBI038_05
#PBI: R1S2EPIC002PBI038
#User Story ID: OOTBF001a, OOTBF001b
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Capture New Request Details - Complete Legal Entity Category

  Scenario: Validate the LOV "Prospect" is removed from "Legal Entity Role" dropdown in all secondary screens 
    Given I login to Fenergo Application with "RM"
    #Select client Type as 'Corporate' in Enter entity details screen
    #When I create a new request with FABEntityType as "NBFI" and LegalEntityRole as "Client/Counterparty"
    #Navigate to Complete screen (3 screen in the workflow)
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "NBFI"
    Then I verify "Prospect" LOV is not presnbt in "Legal Entity Role" dropdown
    And I select "Broker" for "Legal Entity Role" field
    Then validate information message "I Due Diligence is Required" is not displayed
    And I select "Client/Counterparty" for "Legal Entity Role" field
    Then validate information message "I Due Diligence is Required" is not displayed
    And I select "Investment Manager" for "Legal Entity Role" field
    Then validate information message "I Due Diligence is Required" is not displayed
    And I select "Prime Broker" for "Legal Entity Role" field
    Then validate information message "I Due Diligence is Required" is not displayed
    
    