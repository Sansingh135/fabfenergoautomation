#Test Case: TC_R1S2EPIC002PBI038_06
#PBI: R1S2EPIC002PBI038
#User Story ID: OOTBF001a
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Capture New Request Details - Complete Legal Entity Category

  Scenario: Validate the LOV "Prospect" is removed from "Legal Entity Role" dropdown in "Complete" screen and validate if information label/warning messgae is not visilbe for any of th Roles 
	Given I login to Fenergo Application with "RM" 
	When I navigate to "Advanced Search" screen
	When I expand the "Filters" button
	Then I verify "Prospect" LOV is not presnbt in "Legal Entity Role" dropdown
	When I navigate to "Legal Entity Search" screen
	When I expand the "Advanced Search" button
	Then I verify "Prospect" LOV is not presnbt in "Legal Entity Role" dropdown
	