#Test Case: TC_R1S2EPIC005PBI103_02
#PBI: R1S2EPIC005PBI103
#User Story ID: FIG10, FIG11, FIG12, FIG13, FIG14, FIG15
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC005PBI103_02

@To be automated

Scenario: Verify the following functionality on on "Complete ID&V" task for "AML" stage for Onboarding Maker
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty" and "ClientType" as "Corporate"
	When I navigate to "CaptureRequestDetailsFAB" task 
	When I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker"
	When I search for "CaseID" 
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	When I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	When I Right click on hologram to Add Associated Party and add "Individual" type Association and save it
	When I complete "CompleteAMLGrid" task 
	When I navigate to "CompleteID&VGrid" task 
	
	#Test-data: verify Association added to "CompleteAMLGrid" task is displaying under "ID&V" section
	Then I can see already added association under "Associatedparties" section 
	When I click on "Edit" button displaying corresponding to the added association
		
	#Test-data: verify user is able to view LE displayikng under "ID&V" section
	When I navigate to "EditVerification" task 
	Then I can see LE details on "EditVerification" task 
	
	#Test-data: verify user is able to add address for the Associated LE on Edit verification screen
	When I click on "+" sign displaying at the top of "Address" section
	When I navigate to "Address" task 
	When I fill all the details and  complete "Address" task
	Then I can see added Address can be seen under "Address" section on "EditVerification" task for the associated party
	
	#Test-data: verify user is able to add Documents for the Associated LE on Edit verification screen
	When I click on "+" sign displaying at the top of "Documents" section
	When I navigate to "Documents" task 
	When I add document by browsing from "Uploaddocument" button and fill all the details and  complete "Documents" task
	Then I can see added document can be seen under "Documents" section on "EditVerification" task for the associated party
	
	#Test-data: verify user is able to remove Documents for the Associated LE on Edit verification screen
	When I click on "Remove" button through options button displaying corresponding to the added document under "Documents" section 
	Then I can see document is removed under "Documents" section on "EditVerification" task 
	
	#Test-data: verify user is able to view Documents for the Associated LE on Edit verification screen
	When I click on "view" button through options button displaying corresponding to the added document under "Documents" section 
	When I navigate to "Documents" task 
	Then I can see added document and the related details on "Documents" screen
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	