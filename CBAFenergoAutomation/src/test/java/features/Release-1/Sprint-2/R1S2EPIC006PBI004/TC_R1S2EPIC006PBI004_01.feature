#Test Case: TC_R1S2EPIC006PBI004_01
#PBI: R1S2EPIC006PBI004
#User Story ID: US26
#Designed by: Anusha PS
#Last Edited by: Anusha PS
@To_be_automated
Feature: Screening

  Scenario: Validate if "Screening Decision" panel is not visible in Complete AML task - Assessment - Fircosoft Screening	and validate if "Onboarding Maker" is able to add documents and comments
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task as "OnboardingManager"
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    And I assert only the following system-calculated fields are present under "Confirmed Matches" section in the mentioned order
      | Sanctions               |
      | PEP                     |
      | Adverse Media           |
      | Internal Fab Watch List |
    #Add Fircosoft Screening by right clicking the legal entity from hierarchy and clicking "Add Fircosoft Screening"
    And I add Fircosoft Screening for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    And I navigate to "Assessment" screen
    And I validate "Assessment Decision" is not visible between "Active Screenings" and "Screening Tasks" section
    