#Test Case: TC_R1EPIC002PBI010_07
#PBI: R1EPIC002PBI010
#User Story ID: US098
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: Enrich KYC profile task - Anticipated Transaction Sub Flow

Scenario: Validate if Onboarding Maker is able to add mutliple Anticipated Transactions for "Corporate" entity type 

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with ClientEntityType as "Corporate" and LegalEntityrole as "Client/Counterparty"  
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	 Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task  
    When I complete "ValidateKYCandRegulatoryFAB" task
	When I navigate to "EnrichKYCProfileGrid" task 
	
	#Add first Anticipated Transactional Activity (Per Month)
	And I add a "Anticipated Transactional Activity (Per Month)" from "Enrich KYC profile" screen
	And I assert that the added transaction is visible in the grid "Anticipated Transactional Activity (Per Month)" with the following columns:
		|ID | Value of Projected Transactions (AED) | Number of Transactions  | Type/Mode of Transactions | Transaction Type | Currencies Involved | Countries Involved|  
	
	#Add second Anticipated Transactional Activity (Per Month)
	And I add "Anticipated Transactional Activity (Per Month)" from "Enrich KYC profile" screen
	And I assert that the edited transaction is visible in the grid "Anticipated Transactional Activity (Per Month)" with the following columns:
		|ID | Value of Projected Transactions (AED) | Number of Transactions  | Type/Mode of Transactions | Transaction Type | Currencies Involved | Countries Involved|  

	When I complete "EnrichKYCProfileFAB" task 