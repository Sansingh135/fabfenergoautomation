#Test Case: TC_R1EPIC001PBI014_03
#PBI: R1EPIC001PBI014
#User Story ID: Remove/Hide
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: Products Section- Product Information

  Scenario: Verify the below 4 fields are removed in add product screen, add prodcut-trading entity screen, Product search screen and Verified LE details screen
    #Purpose of Account
    #Booking Entity
    #Trading Location
    #Selling Location
    Given I login to Fenergo Application with "RM"
    #Validate in Add product screen
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureNewRequest" task
    When I navigate to "ProductInformation" screen
    And I check that below data is available
      | FieldLabel         | Visible |
      | Purpose of Account | NO      |
      | Booking Entity     | No      |
      | Trading Location   | No      |
      | Selling Location   | No      |
    When I navigate to "tradingentity" screen
    And I check that below data is available
      | FieldLabel         | Visible |
      | Purpose of Account | NO      |
      | Booking Entity     | No      |
      | Trading Location   | No      |
      | Selling Location   | No      |
    #Validate in Product search screen
    When I navigate to "ProductSearch" screen
    And I check that below data is available
      | FieldLabel         | Visible |
      | Purpose of Account | NO      |
      | Booking Entity     | No      |
      | Trading Location   | No      |
      | Selling Location   | No      |
    #Validate in Verified LE Details screen in LE360 screen
    When I navigate to LE360 screen
    And I check that below data is available
      | FieldLabel         | Visible |
      | Purpose of Account | NO      |
      | Booking Entity     | No      |
      | Trading Location   | No      |
      | Selling Location   | No      |
