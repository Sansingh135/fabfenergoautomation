#Test Case: TC_R1EPIC001PBI014_02
#PBI: R1EPIC001PBI014
#User Story ID: US045
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed

Feature: Products Section- Product category and type

@Tobeautomated
Scenario: Validate the 'Product Type' dropdown in Product Search screen 

	Given I login to Fenergo Application with "RM" 
	When I navigate to "ProductSearch" screen
	#Validate product type dropdown is filtered with 37 values when product category is selected as 'Cash Management'
	#Fill in data for product category : Cash management
	And I fill the data for "ProductInformation" with key "CM"
	#Validate the product type lov (37 lovs) as mentioned in the PBI
	And I validate the LOV of "ProductType" with key "CMLov"
	#Validate product type dropdown is filtered with 40 values when product category is selected as 'Corporate Finance'
	#Fill in data for product category : Corporate Finance
	And I fill the data for "ProductInformation" with key "CF"
	#Validate the product type lov (40 lovs) as mentioned in the PBI
	And I validate the LOV of "ProductType" with key "CFLov"
	#Validate product type dropdown is filtered with 25 values when product category is selected as 'Global Markets'
	#Fill in data for product category : Global Markets
	And I fill the data for "ProductInformation" with key "CM"
	#Validate the product type lov (25 lovs) as mentioned in the PBI
	And I validate the LOV of "ProductType" with key "CMLov"
	#Validate product type dropdown is filtered with 15 values when product category is selected as 'Trade Finance'
	#Fill in data for product category : Trade Finance
	And I fill the data for "ProductInformation" with key "TF"
	#Validate the product type lov (15 lovs) as mentioned in the PBI
	And I validate the LOV of "ProductType" with key "TFLov"

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
