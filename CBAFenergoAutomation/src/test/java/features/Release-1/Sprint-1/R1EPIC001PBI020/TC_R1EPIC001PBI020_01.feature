#Test Case: TC_R1EPIC001PBI020_01
#PBI: R1EPIC001PBI020
#User Story ID: US126/SR10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Capture Request details screen-"Telephone number" field
@To be automated 

Scenario: Validate the format of "Telephone number" field on "Capture request details" task of "New Request" stage for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I enter exact 14 numbers in "Telephone number" field in format "CCCNNNNNNNNNNN"
	#Test Data: Enter 14 numbers in "Telephone number" field in format "CCCNNNNNNNNNNN"  under "GLCMS" section 
	Then I can see value in "Telephone number" field is accepted 
	And I save the details successfully