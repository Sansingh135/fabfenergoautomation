#Test Case: TC_R1EPIC001PBI020_05
#PBI: R1EPIC001PBI020
#User Story ID: US126/SR10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Create contact screen- "Home Phone, Work Phone, Mobile" fields
@To be automated 
Scenario: Verify following alphanumeric fields are displaying as per DD (sequence,editable,visible,mandatory) on" Create contact" screen
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I click on "+" sign displaying at the top of contact section to create contact
	When I navigate to "CreateContact" task
	#Test Data: Validate "Home Phone, Work Phone, Mobile" fields are displaying as per DD (sequence,editable,visible,mandatory) on" Create contact" screen
	# refer PBI document for validation
	Then I can see "Home Phone, Work Phone, Mobile" fields are displaying as per DD (sequence,editable,visible,mandatory)
