#Test Case: TC_R1EPIC001PBI020_03
#PBI: R1EPIC001PBI020
#User Story ID: US126/SR10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LE360-overview screen-"Telephone number" field

Scenario: Validate the format of "Telephone number" field on "LE details" task of "LE360-overview" stage for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I enter exact 14 numbers in "Telephone number" field in format "CCCNNNNNNNNNNN"
	#Test Data: Enter 14 numbers in "Telephone number" field in format "CCCNNNNNNNNNNN exact 14 numbers"  under "GLCMS" section 
	Then I can see value in "Telephone number" field is accepted 
	And I save the details successfully
	When I navigate to "LE360overview" task
	Then I can see "Telephone number" field is auto-populated with the value in format "CCCNNNNNNNNNNN exact 14 numbers" under "GLCMS" section