#Test Case: TC_R1EPIC001PBI020_07
#PBI: R1EPIC001PBI020
#User Story ID: US126/SR10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Create Contact screen-"Home Phone, Work Phone, Mobile" fields
@To be automated 

Scenario: Validate the format of "Home Phone, Work Phone, Mobile" fields on "Create contact" task of "New Request" stage
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I navigate to "CreateContact" task by clicking on "+" sign displaying at the top of contact section
	#Test Data: Enter 14 numbers in "Home Phone, Work Phone, Mobile" field in format "CCCNNNNNNNNNNN"  under "GLCMS" section under "GLCMS" section
	When I enter values in "Home Phone, Work Phone, Mobile" fields in format "CCCNNNNNNNNNNN exact 14 numbers" and save the details
	Then I can see values are accepted and saved successfully in the mentioned format