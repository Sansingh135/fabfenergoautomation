#Test Case: TC_R1EPIC001PBI020_08
#PBI: R1EPIC001PBI020
#User Story ID: US126/SR10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LE360-overview screen- "Home Phone, Work Phone, Mobile" fields

Scenario: Verify following Auto-populated fields are displaying under contacts section on "LE360-overview" screen
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I click on "+" sign displaying at the top of contact section to create contact
	When I navigate to "CreateContact" task
	#Test Data: validate values in "Home Phone, Work Phone, Mobile" fields on" Create contact" screen
	When I enter values in "Home Phone, Work Phone, Mobile" fields in format "CCCNNNNNNNNNNN exact 14 numbers" and save the details 
	When I navigate to "LE360overview" task screen
	#Test Data: Validate auto-populated values under contact section on "LE360overview" task
	When I expand "Contacts" section 
	Then I can see values in "Home Phone, Work Phone, Mobile" fields are displaying as auto-populated