#Test Case: TC_R1EPIC01PBI001_06
#PBI: R1EPIC01PBI003
#User Story ID: US032
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB 

@Automation
Scenario: Validate if "Product Details" section is removed on Add Product Screen for "Enrich KYC Profile screen"(using edit button)
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I complete "CaptureRequestDetailsGrid" task
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	When I complete "ValidateKYCandRegulatoryFAB" task
	Given I login to Fenergo Application with "RM"
	When I search for the "CaseId" 
	Then I navigate to "EnrichClientProfileGrid" task
	And I add a Product from "EnrichKYCProfile"
	Then I can see product is visible in Product grid
	When I navigate to product screen again by clicking on "Edit" button
	Then I can see "ProductDetails" section is not visible
	