#Test Case: TC_R1EPIC01PBI001_02
#PBI: R1EPIC01PBI003
#User Story ID: US032
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB 

@Automation
Scenario: Validate if "Product Details" section is removed on Add Product Screen for "Capture Request data screen"(using view button)
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	And I add a Product from "CaptureRequestDetails"
	Then I can see product is visible in Product grid
	When I view the existing Product from "CaptureRequestDetails" 
	Then I can see "ProductDetails" section is not visible
	
