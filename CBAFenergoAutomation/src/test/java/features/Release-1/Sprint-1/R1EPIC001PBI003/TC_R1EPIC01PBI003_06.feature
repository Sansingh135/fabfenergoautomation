#Test Case: TC_R1EPIC01PBI001_07
#PBI: R1EPIC01PBI003
#User Story ID: US032
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate if "Product Details" section is removed for the following users for "Capture Request details screen".
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	When I add a "Products" on "CaptureRequestDetailsGrid" task
	Then I can see product is visible in Product grid
	When I navigate to "Add Product" screen again
	Then I can see "Product details" section is not visible on Add Product screen
	When I store CaseID on LE360 screen
	When I login to Fenergo application with "Onbaordingchecker"
	When I search for the "CaseId" 
	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
	When I navigate to "Add Product" screen again
	Then I can see "Product details" section is not visible on Add Product screen	
	When I store the "CaseId" on LE360 screen 
	When I login to fenergo Appication with "FLODKYCManager"
	When I search for the "CaseId" 
	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
	When I navigate to "Add Product" screen again
	Then I can see "Product details" section is not visible on Add Product screen
	When I store the "CaseId" on LE360 screen 
	When I login to Fenergo Application with "FLODAVP"
	When I search for the "CaseId" 
	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
	When I navigate to "Add Product" screen again
	Then I can see "Product details" section is not visible on Add Product screen
	When I store the "CaseId" on LE360 screen 
	When I login to Fenergo Application with "BusinessUnitHead(N3)"
	When I search for the "CaseId" 
	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
	When I navigate to "Add Product" screen again
	Then I can see "Product details" section is not visible on Add Product screen
	When I store the "CaseId" on LE360 screen 
	When I login to Fenergo Application with "FLODVP"
	When I search for the "CaseId" 
	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
	When I navigate to "Add Product" screen again
	Then I can see "Product details" section is not visible on Add Product screen
	When I store the "CaseId" on LE360 screen 
	When I login to Fenergo Application with "FLODSVP"
	When I search for the "CaseId" 
	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
	When I navigate to "Add Product" screen again
	Then I can see "Product details" section is not visible on Add Product screen
	When I store the "CaseId" on LE360 screen 
	When I login to Fenergo Application with "BusinessHead(N2)"
	When I search for the "CaseId" 
	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
	When I navigate to "Add Product" screen again
	Then I can see "Product details" section is not visible on Add Product screen
	When I store the "CaseId" on LE360 screen 
	When I login to Fenergo Application with "Compliance"
	When I search for the "CaseId" 
	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
	When I navigate to "Add Product" screen again
	Then I can see "Product details" section is not visible on Add Product screen