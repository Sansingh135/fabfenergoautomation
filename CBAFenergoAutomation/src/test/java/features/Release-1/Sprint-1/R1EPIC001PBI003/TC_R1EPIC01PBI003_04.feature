#Test Case: TC_R1EPIC01PBI001_05
#PBI: R1EPIC01PBI003
#User Story ID: US032
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB 

@Automation
Scenario: Validate if "Product Details" section is removed on Add Product Screen for "Enrich KYC Profile screen"(using view button))
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I complete "CaptureRequestDetailsGrid" task
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	When I complete "ValidateKYCandRegulatoryFAB" task 
	And I store the "CaseId" from LE360
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId" 
	When I navigate to "EnrichKYCProfileGrid" task
	And I add a Product from "EnrichKYCProfile"
	Then I can see product is visible in Product grid
	When I view the existing Product from "CaptureRequestDetails"
	Then I can see "ProductDetails" section is not visible
	