#Test Case: TC_R1EPIC01PBI003_01
#PBI: R1EPIC01PBI003
#User Story ID: US032
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: US032

@Automation
Scenario: Validate if RM user is able to see removed "Product Details" section on Add Product Screen.
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	#This is to check that Product section wont be visible when product is not added to the screen already
	When I navigate to AddProduct screen on "CaptureRequestDetails" task
	Then I can see "ProductDetails" section is not visible
	And I close the browser
	
