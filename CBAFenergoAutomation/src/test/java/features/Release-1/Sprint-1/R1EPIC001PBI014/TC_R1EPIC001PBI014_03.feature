#Test Case: TC_R1EPIC001PBI014_03
#PBI: R1EPIC001PBI014
#User Story ID: US024c
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC001PBI014_03

Scenario: Validate "Review Notes" Text-box is displaying as mandatory on "Manager review" screen when "KYC Manager" user refers/Approve the case
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"	
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	When I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
	When I navigate to "QualityControlGrid" task 
	When I complete "QualityControlGrid" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "RM" 
	When I search for "CaseID"
	When I navigate "RelationshipManagerReviewandSign-Off"Grid task 
	#Test data- verify "Review Notes" Text-box is displaying as mandatory on "Relationship Manager Review and Sign-Off" task 
  Then I can see "Review Notes" Text-box is displaying as mandatory
	
	When I complete "ReviewSignOff" task
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODKYCManager" 
	When I search for "CaseID"
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
  #Test data- verify "Review Notes" Text-box is displaying as mandatory on "FLOD KYC Review and Sign-Off" task 
  Then I can see "Review Notes" Text-box is displaying as mandatory

	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODAVP" 
	When I search for "CaseID"
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	#Test data- verify "Review Notes" Text-box is displaying as mandatory on "FLOD AVP Review and Sign-Off" task 
  Then I can see "Review Notes" Text-box is displaying as mandatory
  
	When I complete "FLODAVPReviewandSign-Off" task
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "BusinessUnitHead(N3)" 
	When I search for "CaseID"
	When I navigate to "BUHReviewandSignOffGrid" task 
	#Test data- verify "Review Notes" Text-box is displaying as mandatory on "BUH Review and Sign-Off" task 
  Then I can see "Review Notes" Text-box is displaying as mandatory
	
	And I complete "BUHReviewandSignOff" task by "Submit" button
	
