#Test Case: TC_R1EPIC001PBI014_04
#PBI: R1EPIC001PBI014
#User Story ID: US024c
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC001PBI014_04

Scenario: Validate "Review Notes" Text-box remains non-mandatory on "Manager review" screen for the following users
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"	
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	When I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
  When I complete "RiskAssessmentFAB" task 
  #Test-data: Select risk category as high
  When I select "Riskcategory" as "High
  
	When I complete "RiskAssessmentFAB" task 
	When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
	When I navigate to "QualityControlGrid" task 
	When I complete "QualityControlGrid" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
	When I navigate "RelationshipManagerReviewandSign-Off"Grid task 
	#Test data- verify "Review Notes" Text-box is not displaying as mandatory on "Relationship Manager Review and Sign-Off" task for "Onboardingmaker" user
  Then I can see "Review Notes" Text-box is not displaying as mandatory
	
	When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "Compliance" 
	When I search for "CaseID"
	When I navigate "RelationshipManagerReviewandSign-Off"Grid task 
	#Test data- verify "Review Notes" Text-box is not displaying as mandatory on "Relationship Manager Review and Sign-Off" task for "Compliance" user
  Then I can see "Review Notes" Text-box is not displaying as mandatory
	
	When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FenergoAdmin" 
	When I search for "CaseID"
	When I navigate "RelationshipManagerReviewandSign-Off"Grid task 
	#Test data- verify "Review Notes" Text-box is not displaying as mandatory on "Relationship Manager Review and Sign-Off" task for "FenergoAdmin" user
  Then I can see "Review Notes" Text-box is not displaying as mandatory

  When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODSVP" 
	When I search for "CaseID"
	When I navigate "RelationshipManagerReviewandSign-Off"Grid task 
	#Test data- verify "Review Notes" Text-box is not displaying as mandatory on "Relationship Manager Review and Sign-Off" task for "FLODSVP" user
  Then I can see "Review Notes" Text-box is not displaying as mandatory
  
  When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODAVP" 
	When I search for "CaseID"
	When I navigate "RelationshipManagerReviewandSign-Off"Grid task 
	#Test data- verify "Review Notes" Text-box is not displaying as mandatory on "Relationship Manager Review and Sign-Off" task for "FLODAVP" user
  Then I can see "Review Notes" Text-box is not displaying as mandatory
  
  When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODVP" 
	When I search for "CaseID"
	When I navigate "RelationshipManagerReviewandSign-Off"Grid task 
	#Test data- verify "Review Notes" Text-box is not displaying as mandatory on "Relationship Manager Review and Sign-Off" task for "FLODVP" user
  Then I can see "Review Notes" Text-box is not displaying as mandatory
  
  When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "Onboardingchecker" 
	When I search for "CaseID"
	When I navigate "RelationshipManagerReviewandSign-Off"Grid task 
	#Test data- verify "Review Notes" Text-box is not displaying as mandatory on "Relationship Manager Review and Sign-Off" task for "Onboardingchecker" user
  Then I can see "Review Notes" Text-box is not displaying as mandatory
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  