#Test Case: TC_R1EPIC002PBI016_0
#PBI: R1EPIC002PBI016
#User Story ID: US055,US056,US057, US058, US059
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: Internal Booking Entity section

Scenario: Validate new fields are added under "Internal Booking details" section on "Capture request details" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I complete "CaptureRequestDetailsFAB" task 	
	When I navigate to "ReviewRequest" task	
	#Test Data:Verify "Target Code","Sector Description","UID Originating Branch","Propagate To Target Systems" drop-down fields are getting displayed on "Review request"" screen	
	Then I can see "Target Code","Sector Description","UID Originating Branch","Propagate To Target Systems" drop-down fields are getting displayed