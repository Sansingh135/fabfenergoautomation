#Test Case: TC_R1EPIC002PBI016_02
#PBI: R1EPIC002PBI016
#User Story ID: USR01
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: Internal Booking Entity section

@To be automated 

Scenario: Validate the removed fields under "Internal Booking details" section on "Capture request details" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I complete "CaptureRequestDetailsFAB" task 	
	When I navigate to "ReviewRequest" task
	#Test Data:Verify "Priority","From Office Area","On Behalf Of","Internal desk" drop-doenfields should be display as removed on "Review request" screen	
	Then I can see "Priority","From Office Area","On Behalf Of","Internal desk" drop-down fields displaying as removed