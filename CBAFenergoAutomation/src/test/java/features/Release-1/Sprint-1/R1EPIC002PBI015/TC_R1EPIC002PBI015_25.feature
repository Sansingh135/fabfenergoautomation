#Test Case: TC_R1EPIC002PBI015_25
#PBI: R1EPIC002PBI015
#User Story ID: US100
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate non-mandatory field "Country of Incorporation/Establishment" is displaying on "Enter Entity details" and "Search for duplicates" screen
	Given I login to Fenergo application with "RM" user
	When I click on "+" sign to create new request
	When I navigate to "Enter Entity details" screen task
	#Test data: Verify non-mandatory field "Country of Incorporation/Establishment" is displaying on "Enter Entity details" screen
	#refer DD for (Sequence, Mandatory, Editable, visible)
	Then I can see non-mandatory field "Country of Incorporation/Establishment" is displaying on "Enter Entity details" screen
	When I complete "Enter Entity details" screen task
	When I navigate to "Searchforduplicates" screen
	#Test data: Verify non-mandatory field "Country of Incorporation/Establishment" is displaying on "Search for duplicates" screen
	Then I can see non-mandatory field "Country of Incorporation/Establishment" is displaying