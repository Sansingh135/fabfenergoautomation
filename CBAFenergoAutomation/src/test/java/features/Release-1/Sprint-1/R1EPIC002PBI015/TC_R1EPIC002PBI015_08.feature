#Test Case: TC_R1EPIC002PBI015_08
#PBI: R1EPIC002PBI015
#User Story ID: US113
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate input for "Legal Entity Name" is as per DD on "Enter Entity details" screen of "New request" stage
	Given I login to Fenergo application with "RM" user
	When I click on "+" sign to create new request
	When I navigate to "Enter Entity details" screen
	When I enter value in "LegalEntityName"
	When I navigate to "Search for Duplicates" screen by clicking on "search" button
	When I edit value in "LegalEntityName" upto 255 characters
	#Test Data:validate (sequence,LOV,Visibility/editability/mandatory) of "Legal entity Name" field is as per DD and should should accept till 255 characters
	Then I can see "LegalEntityName" field (sequence,LOV,Visibility/editability/mandatory) is displaying as mentioned in DD
	Then I see value in "LegalEntityName" field value is saved successfully