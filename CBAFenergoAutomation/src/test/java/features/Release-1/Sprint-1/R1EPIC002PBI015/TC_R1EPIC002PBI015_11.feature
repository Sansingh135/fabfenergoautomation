#Test Case: TC_R1EPIC002PBI015_011
#PBI: R1EPIC002PBI015
#User Story ID: US106
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB 


@Automation
Scenario: Validate mandatory field "Entity Type" drop-down on "Enter Entity details" screen of "New request" stage
	Given I login to Fenergo Application with "RM"
	When I click on "PlusButton" sign to create new request
	
	#Test Data:Field "Entity Type" drop-down is displaying as Mandatory on "Enter Entity details" screen of "New request" stage
	Then I check that "Entity Type" is Mandatory