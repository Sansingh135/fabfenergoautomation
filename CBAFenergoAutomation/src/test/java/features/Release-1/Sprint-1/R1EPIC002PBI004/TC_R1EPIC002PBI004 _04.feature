#Test Case: TC_R1EPIC002PBI004_04
#PBI: R1EPIC002PBI004
#User Story ID: US024
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI004_04
  @TobeAutomated
  Scenario: Verify the conditionally available field behaviour of new fields when Client type is PCG Entity
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = UAE and client type = PCG Entity
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I check that below data is available
      | FieldLabel            | Field Type | Visible | Mandatory | Editable | Field Defaults to |
      | Channel and Interface | Dropdown   | Yes     | Yes       | Yes      | Select...         |
