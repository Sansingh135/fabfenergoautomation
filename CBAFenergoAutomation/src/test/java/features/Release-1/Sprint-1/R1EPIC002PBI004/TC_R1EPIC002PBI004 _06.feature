#Test Case: TC_R1EPIC002PBI004_06
#PBI: R1EPIC002PBI004
#User Story ID: Remove/hide
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI004_06

  Scenario: Verify the below fields are removed in Capture request details and Review request Screen
    #Registration Number
    #Countries of Business Operations/Economic Activities
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with legal entity role as Client/Counterparty
    And I check that below data is available
      | FieldLabel                                           | Visible |
      | Countries of Business Operations/Economic Activities | NO      |
      | Registration Number                                  | No      |
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    #Verify the below fields are removed in review request screen
    And I check that below data is available
      | FieldLabel                                           | Visible |
      | Countries of Business Operations/Economic Activities | NO      |
      | Registration Number                                  | No      |
