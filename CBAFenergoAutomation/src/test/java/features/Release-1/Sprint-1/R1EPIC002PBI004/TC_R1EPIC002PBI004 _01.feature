#Test Case: TC_R1EPIC002PBI004_01
#PBI: R1EPIC002PBI004
#User Story ID: US068, US024, US025, US087, US040, US039, US033, US041
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: Capture New Request Details - Customer Details.
  @TobeAutomated
  Scenario: Verify the field behaviour of below new fields added in Customer Details section of Capture Request Details Screen
    #Legal Entity Category
    #Purpose of Account/Relationship
    #Residential Status
    #Justification for opening/maintaining non-resident account
    #Customer Tier
    #Relationship with bank
    #CIF Creation Required?
    #Emirate
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = UAE and client type = corporate
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I check that below data is available
      | FieldLabel                                                 | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Legal Entity Category                                      | Dropdown   | Yes       | No       | Autopopulated     | Yes     |
      | Channel and Interface                                      | Dropdown   | Yes       | Yes      | Select...         | Yes     |
      | Purpose of Account/Relationship                            | Textbox    | No        | Yes      |                   | Yes     |
      | Residential Status                                         | Dropdown   | Yes       | No       |                   | Yes     |
      | Customer Tier                                              | Dropdown   | Yes       | Yes      | Select...         | Yes     |
      | Relationship with bank                                     | Dropdown   | Yes       | Yes      | Select...         | Yes     |
      | CIF Creation Required?                                     | Checkbox   | Yes       | Yes      | Yes               | Yes     |
      | Emirate                                                    | Dropdown   | Yes       | Yes      | Select...         | Yes     |
      | Justification for opening/maintaining non-resident account | Textbox    | Yes       | Yes      |                   | No      |
    #Verify Residential status is set to Resident when country of Incorporation = UAE
    And I check that below data is available
      | FieldLabel         | Value    |
      | Residential Status | Resident |
    #Validate the Legal Category Type lovs (LegalEntityCategory-Refer to LOV tab in the PBI)
    And I validate the LOV of "LegalEntityCategory" with key "Legalentiycatlov"
    #Validate the 'Channel & Interface' lovs
    Then I validate the specific LOVs for "Channel & Interface"
      | Lovs                                                                        |
      | Over the counter/Face-to-face (direct banking, branch/relationship manager) |
      | Non-face-to-face (agents, representative, third parties)                    |
      | Others                                                                      |
    #Validate the Residential Status lovs (Residency Status-Refer to LOV tab in the PBI)
    And I validate the LOV of "ResidencyStatus" with key "resstatuslov"
    #Validate the Customer Tier lovs (Customer Tier-Refer to LOV tab in the PBI)
    And I validate the LOV of "CustomerTier" with key "custierlov"
    #Validate the Relationship with bank lovs (Relationship with Bank-Refer to LOV tab in the PBI)
    And I validate the LOV of "Relationshipwithbank" with key "relwithbanklov"
    #Validate the emirate lovs (Emirates-Refer to LOV tab in the PBI)
    And I validate the LOV of "Emirate" with key "emiratelov"
    #Verify the field behavior of new fields in review request screen
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    And I check that below data is available
      | FieldLabel                                                 | Field Type | Mandatory | Editable |
      | Legal Entity Category                                      | Dropdown   | Yes       | No       |
      | Channel and Interface                                      | Dropdown   | Yes       | No       |
      | Purpose of Account/Relationship                            | Textbox    | No        | No       |
      | Residential Status                                         | Dropdown   | Yes       | No       |
      | Justification for opening/maintaining non-resident account | Textbox    | Yes       | No       |
      | Customer Tier                                              | Dropdown   | Yes       | No       |
      | Relationship with bank                                     | Dropdown   | Yes       | No       |
      | CIF Creation Required?                                     | Checkbox   | Yes       | No       |
      | Emirate                                                    | Dropdown   | Yes       | No       |
