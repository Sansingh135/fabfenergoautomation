#Test Case: TC_R1EPIC002PBI005_03
#PBI: R1EPIC002PBI005
#User Story ID: US089,US093,US041,US092
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: Enrich KYC profile task -Customer Details

  @TobeAutomated
  Scenario: Verify the conditionally available field behaviour of new fields added in Customer Details section of Enrich KYC Profile Screen
 	#Entity Level
 	#Emirate
 	#Previous Name(s)
 	#Swift Address
 	  #Create entity with Entity type = FI or NBFI and Country of Incorporation = UAE and set 'yes' for the field 'Does the entity have a previous name(s)?'
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create new request with LegalEntityrole as "Client/Counterparty"
    ###	And I fill the data for "CaptureNewRequest" with key "C1"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    And I check that below data is available
    #Entity level field will be Conditionality available when FAB Entity Type = FI or NBFI
    #Emirate field will be available and read only at this stage if country of incorporation is UAE
    #Previous Name(s) filed will be Visible and mandatoy if Does the entity have a previous name(s)? = Yes
    #Swift Address field will be visible when FAB Entity type = FI or NBFI
    And I check that below data is available
      | FieldLabel                   | Field Type | Mandatory | Editable | Field Defaults to |
      | Previous Name(s)             | Textbox    | Yes       | Yes      |                   |
      | Entity Level                 | Dropdown   | Yes       | Yes      |                   |
      | Emirate                      | Dropdown   | Yes       | No       | Select...         |
      | SWIFT Address                | Textbox    | Yes       | Yes      |                   |
   
