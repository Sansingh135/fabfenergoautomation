#Test Case: TC_R1EPIC002PBI005_05
#PBI: R1EPIC002PBI005
#User Story ID: Remove/hide
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: Enrich KYC profile task -Customer Details

  Scenario: Verify the below 4 fields are removed in Customer Details section of Enrich KYC Profile Screen
    #Countries of Business Operations/Economic Activities
    #Date of last name change
    #Legal Status
    #Can the corporation issue bearer shares?
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create new request with LegalEntityrole as "Client/Counterparty"
    ###	And I fill the data for "CaptureNewRequest" with key "C1"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    And I check that below data is available
      | FieldLabel                                           | Visible |
      | Countries of Business Operations/Economic Activities | NO      |
      | Date of last name change                             | No      |
      | Legal Status                                         | No      |
      | Can the corporation issue bearer shares?             | No      |
