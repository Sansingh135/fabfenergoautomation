#Test Case: TC_R1EPIC002PBI005_01
#PBI: R1EPIC002PBI005
#User Story ID: US091, US086, US088, US030, US036, US034, US035, US038, US037, US090
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: Enrich KYC profile task -Customer Details

  @TobeAutomated
  Scenario: Verify the field behaviour of below 14 new fields added in Customer Details section of Enrich KYC Profile Screen
    #FAB Segment
    #Customer Relationship Status
    #Length of Relationship
    #Legal Entity Name (Parent)
    #Website Address
    #Real Name
    #Original Name
    #Legal Counter party type
    #Legal Constitution Type
    #Group Name
    #Does the entity have a previous name(s)?
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create new request with LegalEntityrole as "Client/Counterparty"
    ###	And I fill the data for "CaptureNewRequest" with key "C1"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    And I check that below data is available
      | FieldLabel                               | Field Type | Mandatory | Editable | Field Defaults to |
      | FAB Segment                              | Dropdown   | Yes       | Yes      |                   |
      | Customer Relationship Status             | Dropdown   | No        | No       |                   |
      | Length of Relationship                   | Dropdown   | Yes       | Yes      | New Customer      |
      | Legal Entity Name (Parent)               | Textbox    | No        | Yes      |                   |
      | Website Address                          | Textbox    | No        | Yes      |                   |
      | Real Name                                | Textbox    | No        | Yes      |                   |
      | Original Name                            | Textbox    | No        | Yes      |                   |
      | Legal Counter party type                 | Dropdown   | Yes       | Yes      | Select...         |
      | Legal Constitution Type                  | Dropdown   | Yes       | Yes      | Select...         |
      | Group Name                               | Textbox    | No        | Yes      |                   |
      | Does the entity have a previous name(s)? | Dropdown   | Yes       | Yes      | Select...         |
    #Validate the FAB Segments lovs (FABsegmentcode&desc-Refer to LOV tab in the PBI)
    And I validate the LOV of "FABsegmentcode" with key "fabseglov"
    #Validate the Customer Relationship Status lovs (Customer Relationship Status-Refer to LOV tab in the PBI)
    And I validate the LOV of "CustomerRelationshipStatus" with key "cusrelstatlov"
    #Validate the Length of Relationship lovs (LengthofRelationship-Refer to LOV tab in the PBI)
    And I validate the LOV of "LengthofRelationship" with key "lenofrellov"
    #Validate the Legal Counter party type lovs (Legal Counterparty Type-Refer to LOV tab in the PBI)
    And I validate the LOV of "LegalCounterpartyType" with key "legcoupartylov"
    #Validate the Legal Constitution Type lovs (Legal Constitution Type-Refer to LOV tab in the PBI)
    #Dynamic list displayed based on Legal Counterparty type selected
    And I validate the LOV of "LegalConstitutionType" with key "legconstypelov"
    #Verify Legal Entity Name (parent) field NOT accepts special characters. Test Data:Special characters
    And I fill the data for "enrichkycprofile" with key "Data1"
    #Verify Legal Entity Name (parent) field NOT accepts lower case characters. Test Data: Lower case char
    And I fill the data for "enrichkycprofile" with key "Data2"
    #Verify Legal Entity Name (parent) field accepts valid characters other than special and lowercase.
    And I fill the data for "enrichkycprofile" with key "Data3"
    #Verify Website field does not accept duplicate value
    And I fill the data for "enrichkycprofile" with key "Data4"
