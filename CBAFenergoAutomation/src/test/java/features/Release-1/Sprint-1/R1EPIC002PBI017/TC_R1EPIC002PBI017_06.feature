#Test Case: TC_R1EPIC002PBI017_06
#PBI: R1EPIC002PBI017
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LE Verified details- "Customer details" and "Internal Booking details" section

@To be automated 

Scenario: Validate removed fields under "Customer details" and "Internal Booking details" section on "LE Verified details" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	Then I fill values in all mentioned fields and complete "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker"
	When I search for "CaseID" 
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	When I navigate to "QualityControlGrid" task 
	When I complete "ReviewOnboarding" task 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	When I complete "FLODAVPReviewandSign-Off" task
	When I navigate to "BUHReviewandSignOffGrid" task 
	When I complete "BUHReviewandSignOff" task
	When I navigate to "CaptureFabReferencesGrid" task
	When I complete "CaptureFabReferences" task
	#Validating that the case status is closed		  
	When I navigate to "LEverifieddetails" task screen by navigating through "LE360-overview" screen
	When I navigate to "Customer details" and "Internal Booking details" section
	#Test Data:Verify new auto-populated fields under "Internal Booking details" section on "LEverifieddetails" screen		
	And I validate the following removed fields under "Customer details" and "Internal Booking details" section

		-Legal status
		-Internal desk
	
	
	
	
	
	
	
	 		