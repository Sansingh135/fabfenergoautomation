#Test Case: TC_R1EPIC002PBI017_09
#PBI: R1EPIC002PBI017
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LEverified details screen- "customer details" section

Scenario: Validate new auto-populated fields under "Customer details" section on "LEVerified details" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	Then I fill values in all mentioned fields and complete "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	When I navigate to "QualityControlGrid" task 
	When I complete "ReviewOnboarding" task 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	When I complete "FLODAVPReviewandSign-Off" task
	When I navigate to "BUHReviewandSignOffGrid" task 
	When I complete "BUHReviewandSignOff" task
	When I navigate to "CaptureFabReferencesGrid" task
	When I complete "CaptureFabReferences" task
	#Validating that the case status is closed	
	
	When I navigate to "LE360-overview" task
	When I navigate to "customer details" section by clicking on "LEverifieddetails" grid
	
  #Test Data:Verify renamed fields under  "customer details" section on "LEverifieddetails" screen		
	And I validate the following Auto-populated fields under "customer details" section
	-FAB Segment
	-Channel & Interface
	-Residential Status
	-Customer Tier
	-Relationship with bank
	-CIF Creation Required?
	-Emirate
	-Customer Relationship Status
	-Length of Relationship
	-Entity Level
	-Legal Counter party type
	-Legal Constitution Type
	-Purpose of Account/Relationship
	-Justification for opening/maintaining non-resident account
	-Previous Name(s)
	-Legal Entity Name (Parent)
	-Website Address
	-Real Name
	-Original Name
	-SWIFT Address
	-Group name			
				