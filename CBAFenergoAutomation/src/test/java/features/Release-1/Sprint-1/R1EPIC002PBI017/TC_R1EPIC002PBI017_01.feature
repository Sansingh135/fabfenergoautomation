#Test Case: TC_R1EPIC002PBI017_01
#PBI: R1EPIC002PBI017
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LE360 overview- "Customer details" and "internal Booking" details section

Scenario: Validate the renamed fields under "Customer details" section on "LE360-overview" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	#Test Data:Verify renamed fields under"Internal Booking details" section on "Capture request details" screen	
	
	And I validate the following renamed fields under "Internal Booking details" section
		Current Label     			          |Renamed label						
		Principal Place of Business 			|Countries of Business Operations/Economic Activity 						
		FAB Entity Type										|Client Type						
		Registration body				 					|Name of Registration body				
		Country of Domicile								|Country of Domicile/Physical Presence				
		Operating/Trading Name						|Trading/Operation Name  			
		Entity of Onboarding							|Booking Country   			
	Then I fill values in all mentioned fields and complete "CaptureRequestDetailsFAB" task 
	When I navigate to "LE360-overview" task
	When I navigate to "Internal Booking details" section by clicking on "LEdetails" grid
	
  #Test Data:Verify renamed fields under"Internal Booking details" section on "LE360-overview" screen		
	And I validate the following renamed fields under "Internal Booking details" section
		Current Label     			          |Renamed label						
		Principal Place of Business 			|Countries of Business Operations/Economic Activity 						
		FAB Entity Type										|Client Type						
		Registration body				 					|Name of Registration body				
		Country of Domicile								|Country of Domicile/Physical Presence				
		Operating/Trading Name						|Trading/Operation Name  			
		Entity of Onboarding							|Booking Country   		