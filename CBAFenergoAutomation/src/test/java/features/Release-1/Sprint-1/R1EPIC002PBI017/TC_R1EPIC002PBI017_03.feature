#Test Case: TC_R1EPIC002PBI030_03
#PBI: R1EPIC002PBI030
#User Story ID: US074
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LE360 overview- "Anticipated Transactional Activity (Per Month)" section

Scenario: Validate new auto-populated fields under "Anticipated Transactional Activity (Per Month)" section on "LE360-overview" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" and "ClientType" as "Corporate"
	When I navigate to "CaptureRequestDetailsFAB" task 
	Then I fill values in all mentioned fields and complete "CaptureRequestDetailsFAB" task 
	When I navigate to "LE360-overview" task
	When I navigate to "Anticipated Transactional Activity (Per Month)" section by clicking on "LEdetails" grid
	#Test Data:Verify new auto-populated fields under "Anticipated Transactional Activity (Per Month)" section on "LE360-overview" screen		
	And I validate the following new fields under "Anticipated Transactional Activity (Per Month)" section

	-Value of Projected Transactions (AED)
  -Number of Transactions 
  -Type/Mode of Transactions
  -Transaction Type
  -Currencies Involved
  -Countries Involved  						   		