#Test Case: TC_R1EPIC002PBI017_02
#PBI: R1EPIC002PBI017
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LE360 overview- Customer details section

@To be automated 

Scenario: Validate new auto-populated fields under "Customer details" section on "LE360-overview" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 
	Then I fill values in all mentioned fields and complete "CaptureRequestDetailsFAB" task 
	When I navigate to "LE360-overview" task
	When I navigate to "Customer details" section by clicking on "LEdetails" grid
	#Test Data:Verify new auto-populated fields under"Customer details" section on "LE360-overview" screen		
	And I validate the following new fields under "Customer details" section

	-FAB Segment
	-Channel & Interface
	-Residential Status
	-Customer Tier
	-Relationship with bank
	-CIF Creation Required?
	-Emirate
	-Customer Relationship Status
	-Length of Relationship
	-Entity Level
	-Legal Counter party type
	-Legal Constitution Type
	-Purpose of Account/Relationship
	-Justification for opening/maintaining non-resident account
	-Previous Name(s)
	-Legal Entity Name (Parent)
	-Website Address
	-Real Name
	-Original Name
	-SWIFT Address
	-Group name							   		