#Test Case: TC_R1EPIC001PBI013_03
#PBI: R1EPIC001PBI013
#User Story ID: US034
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LE verified details screen-"Anticipated Activity of Account" field

Scenario: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section of "Enrich client Profile screen" stage for onboarding maker.
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I complete "CaptureRequestDetailsFAB" task 
	When I complete to "ReviewRequest" task	
	Then I store the "CaseId" from LE360
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task	
	When I complete "ValidateKYCandRegulatoryGrid" task	
	When I navigate to "EnrichKYCProfileGrid" task
	When I complete "EnrichKYCProfile" task 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "RiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	
	#Login with different user due to maker/checker roles
	Given I login to Fenergo Application with "Superuser2" 
	When I search for the "CaseId" 
	When I navigate to "ReviewOnboardingGrid" task 
	When I complete "ReviewOnboarding" task
		
	#	Validating that the case status is closed
	And I assert that the CaseStatus is "Closed" 	
	When I navigate to "LEVerifieddetails" task of "LE360-overview" screen		
	#Test Data:Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible
	
	When I store CaseID on LE360 screen
	When I login to Fenergo application with "Onbaordingchecker"
	When I search for the "CaseID"
	When I navigate to "LEVerifieddetails" task of "LE360-overview" screen
	#Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible
	
	When I store the "CaseId" on LE360 screen 
	When I login to fenergo Appication with "FLODKYCManager"
	When I search for the "CaseId" 
	When I navigate to "LEVerifieddetails" task of "LE360-overview" screen
	#Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible
	
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "FLODAVP"	
	When I search for the "CaseId" 
	When I navigate to "LEVerifieddetails" task of "LE360-overview" screen
	#Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible
	
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "BusinessUnitHead(N3)"
	When I search for the "CaseId" 
	When I navigate to "LEVerifieddetails" task of "LE360-overview" screen
	#Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible
		
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "FLODVP"
	When I search for the "CaseId" 
	When I navigate to "LEVerifieddetails" task of "LE360-overview" screen
	#Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible
	
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "FLODSVP"
	When I search for the "CaseId" 
	When I navigate to "LEVerifieddetails" task of "LE360-overview" screen
	#Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible
	
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "BusinessHead(N2)"
	When I search for the "CaseId" 
	When I navigate to "LEVerifieddetails" task of "LE360-overview" screen
	#Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible
	
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "Compliance"
	When I search for the "CaseId" 
When I navigate to "LEVerifieddetails" task of "LE360-overview" screen
	#Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	