#Test Case: TC_R1EPIC002PBI030_01
#PBI: R1EPIC002PBI030
#User Story ID: US072
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: "Enrich client profile", "LE360 Overview", "Le verified details"- "business details" section

@To be automated 

Scenario: Validate "Nature of Activity / Business" is renamed as "Nature of Activity Business" under "Business details" section for Onboarding Maker user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  # Test data- Validate "Nature of Activity / Business" is renamed as "Nature of Activity Business" and size (length upto 1000 characters) under "Business details" section
  Then I can see "Nature of Activity / Business" is renamed as "Nature of Activity Business" and as per DD(sequence, mandatory, editable, Visible) under "Business details" section
  And I validate value in "Nature of Activity Business" field as "length upto 1000 characters" and save the details successfully.
  
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	When I navigate to "QualityControlGrid" task 
	When I complete "ReviewOnboarding" task 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	When I complete "FLODAVPReviewandSign-Off" task
	When I navigate to "BUHReviewandSignOffGrid" task 
	When I complete "BUHReviewandSignOff" task
	When I navigate to "CaptureFabReferencesGrid" task
	When I complete "CaptureFabReferences" task
	#Validating that the case status is closed
		
	When I navigate to "LE360-overview" task
	When I navigate to "Businessdetails" section by clicking on "LEdetails" grid
	
	# Test data- Validate "Nature of Activity / Business" is renamed as "Nature of Activity Business" under "Business details" section on "LE360-overview" task
  Then I can see "Nature of Activity / Business" is renamed as "Nature of Activity Business" under "Business details" section
  And the value in "Nature of Activity Business" field is auto-populated same from "EnrichKYCProfileGrid" task
  
  When I navigate to "LEVerifieddetails" task by navigating through "LE360-overview" stage
	When I navigate to "Businessdetails" section
	
	# Test data- Validate "Nature of Activity / Business" is renamed as "Nature of Activity Business" under "Business details" section on "LEVerifieddetails" task
  Then I can see "Nature of Activity / Business" is renamed as "Nature of Activity Business" under "Business details" section
  And the value in "Nature of Activity Business" field is auto-populated same from "EnrichKYCProfileGrid" task
  
  
  
  
  
     		