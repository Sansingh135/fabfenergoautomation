#Test Case: TC_R1EPIC002PBI030_12
#PBI: R1EPIC002PBI030
#User Story ID: US080,US081
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: "Enrich client profile"- "Source Of Funds And Wealth Details" section

@To be automated 

Scenario: Validate for below mentioned conditional fields under "Source Of Funds And Wealth Details" section on "Enrich KYC Profile" task for Onboarding Maker
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" and client Type "Corporate" or "PCG-Entity" 
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  # Test data- Validate below mentioned conditional fields are visible under "Source Of Funds And Wealth Details" section
   Then I validate below mentioned fields are visible and their LOVs are as per DD.
     
 	 	-Projected Annual Business Turnover (AED)
		-Annual Business Turnover of Group (AED)