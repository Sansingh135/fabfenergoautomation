#Test Case: TC_R1EPIC002PBI030_03
#PBI: R1EPIC002PBI030
#User Story ID: US074
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: "Enrich client profile", "LE360 Overview", "Le verified details"- "Source Of Funds And Wealth Details" section

@To be automated 

Scenario: Validate "Legal Entity Source of Wealth" is renamed as "Legal Entity Source of Income & Wealth" under "Source Of Funds And Wealth Details" section for Onboarding Maker user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" and Client Type is "Corporate" "PCG-Entity" or "NBFI"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  # Test data- I validate "Legal Entity Source of Wealth" is renamed as "Legal Entity Source of Income & Wealth" and size (length upto 1000 characters) under "Source Of Funds And Wealth Details" section
  Then I can see "Legal Entity Source of Wealth" is renamed as "Legal Entity Source of Income & Wealth" and as per DD(sequence, mandatory, editable, Visible) under "Business details" section
  And I validate value in "Legal Entity Source of Income & Wealth" field as "length upto 1000 characters" and save the details successfully.
  
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	When I navigate to "QualityControlGrid" task 
	When I complete "ReviewOnboarding" task 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	When I complete "FLODAVPReviewandSign-Off" task
	When I navigate to "BUHReviewandSignOffGrid" task 
	When I complete "BUHReviewandSignOff" task
	When I navigate to "CaptureFabReferencesGrid" task
	When I complete "CaptureFabReferences" task
	#Validating that the case status is closed
		
	When I navigate to "LE360-overview" task
	When I navigate to "Source Of Funds And Wealth Details" section by clicking on "LEdetails" grid
	
	# Test data- I validate "Legal Entity Source of Wealth" is renamed as "Legal Entity Source of Income & Wealth" under "Source Of Funds And Wealth Details" section on "LE360-overview" task
  Then I can see "Legal Entity Source of Wealth" is renamed as "Legal Entity Source of Income & Wealth" under "Source Of Funds And Wealth Details" section
  And the value in "Legal Entity Source of Income & Wealth" field is auto-populated same from "EnrichKYCProfileGrid" task
  
  When I navigate to "LEVerifieddetails" task by navigating through "LE360-overview" stage
	When I navigate to "Source Of Funds And Wealth Details" section
	
	# Test data- I validate "Legal Entity Source of Wealth" is renamed as "Legal Entity Source of Income & Wealth" under "Source Of Funds And Wealth Details" section on "LEVerifieddetails" task
  Then I can see "Legal Entity Source of Wealth" is renamed as "Legal Entity Source of Income & Wealth" under "Source Of Funds And Wealth Details" section
  And the value in "Legal Entity Source of Income & Wealth" field is auto-populated same from "EnrichKYCProfileGrid" task
  
  
  
  
  
     		