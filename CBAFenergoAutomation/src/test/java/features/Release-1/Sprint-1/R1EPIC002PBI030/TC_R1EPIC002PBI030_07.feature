#Test Case: TC_R1EPIC002PBI030_07
#PBI: R1EPIC002PBI030
#User Story ID: US075, US083
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: "Enrich client profile"- "business details" section

@To be automated 

Scenario: Validate LOVs for below mentioned fields under "Business details" section on "Enrich KYC Profile" task for Onboarding Maker
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  # Test data- Validate LOVs for "Anticipated Transactions Turnover (Annual in AED)" and "Active Presence in Sanctioned Countries/Territories" under "Business details" section
   And I validate LOVs for below fields from DD and save 
   
   	-Anticipated Transactions Turnover (Annual in AED)
		-Active Presence in Sanctioned Countries/Territories 
  