#Test Case: TC_R1EPIC002PBI030_15
#PBI: R1EPIC002PBI030
#User Story ID: US085
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: "Enrich client profile"- "Source Of Funds And Wealth Details" section

Scenario: Validate for conditional field "Offshore Banking License" under "Source Of Funds And Wealth Details" section on "Enrich KYC Profile" task for Onboarding Maker
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" and client Type "FI"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  # Test data- Validate conditional field "Offshore Banking License" is visible and its LOVs are as per DD under "Source Of Funds And Wealth Details" section
   Then I validate "Offshore Banking License" field is visible
   And I validate the LOVs for "Offshore Banking License" field as per DD and save 
   
  