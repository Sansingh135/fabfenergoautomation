#Test Case: TC_R1EPIC002PBI030_11
#PBI: R1EPIC002PBI030
#User Story ID: US079
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: "Enrich client profile"- "Source Of Funds And Wealth Details" section

Scenario: Validate for conditional field "Annual Business Turnover (AED)" under "Source Of Funds And Wealth Details" section on "Enrich KYC Profile" task for Onboarding Maker
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" and client Type "Corporate" or "PCG-Entity" or "NBFI"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  # Test data- Validate conditional field "Annual Business Turnover (AED)" is visible and its LOVs are as per DD under "Source Of Funds And Wealth Details" section
   Then I validate "Annual Business Turnover (AED)" field is visible
   And I validate the LOVs for "Annual Business Turnover (AED)" field as per DD and save 
   
  