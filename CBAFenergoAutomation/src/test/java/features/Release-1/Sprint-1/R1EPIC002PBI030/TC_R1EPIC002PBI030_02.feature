#Test Case: TC_R1EPIC002PBI030_02
#PBI: R1EPIC002PBI030
#User Story ID: US073
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: "Enrich client profile", "LE360 Overview", "Le verified details"- "Source Of Funds And Wealth Details" section

@To be automated 

Scenario: Validate behaviour of "Legal Entity Source of Funds" field under "Source Of Funds And Wealth Details" section for Onboarding Maker user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" and Client Type is "Corporate" or "PCG-Entity"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  # est data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
  Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
  
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	When I navigate to "QualityControlGrid" task 
	When I complete "ReviewOnboarding" task 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	When I complete "FLODAVPReviewandSign-Off" task
	When I navigate to "BUHReviewandSignOffGrid" task 
	When I complete "BUHReviewandSignOff" task
	When I navigate to "CaptureFabReferencesGrid" task
	When I complete "CaptureFabReferences" task
	#Validating that the case status is closed
		
	When I navigate to "LE360-overview" task
	When I navigate to "Source Of Funds And Wealth Details" section by clicking on "LEdetails" grid
	
# est data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
  Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
  
  
  When I navigate to "LEVerifieddetails" task by navigating through "LE360-overview" stage
	When I navigate to "Source Of Funds And Wealth Details" section
	
# est data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
  Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
 
 # Validating "Legal Entity Source of Funds" for client type "FI" or "NBFI" and Product is "Call Account" or "Savings account"
 Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" and Client Type is "FI" or "NBFI"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "Onboardingmaker" 
	When I search for "CaseID"
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  When I add product type "Call Account" or "Savings account" and save 
  
  # Test data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
  Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
  
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	When I navigate to "QualityControlGrid" task 
	When I complete "ReviewOnboarding" task 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	When I complete "FLODAVPReviewandSign-Off" task
	When I navigate to "BUHReviewandSignOffGrid" task 
	When I complete "BUHReviewandSignOff" task
	When I navigate to "CaptureFabReferencesGrid" task
	When I complete "CaptureFabReferences" task
	#Validating that the case status is closed
		
	When I navigate to "LE360-overview" task
	When I navigate to "Source Of Funds And Wealth Details" section by clicking on "LEdetails" grid
	
# Test data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
  Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
  
  When I navigate to "LEVerifieddetails" task by navigating through "LE360-overview" stage
	When I navigate to "Source Of Funds And Wealth Details" section
	
# Test data- I validate "Legal Entity Source of Funds" field is visible under "Source Of Funds And Wealth Details" section
  Then I can see "Legal Entity Source of Funds" is visible as per DD(sequence, mandatory, editable, Visible) under "Source Of Funds And Wealth Details" section
  
  
     		