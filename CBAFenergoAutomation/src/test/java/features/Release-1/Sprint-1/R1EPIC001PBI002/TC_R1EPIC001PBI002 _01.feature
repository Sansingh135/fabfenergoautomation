#Test Case: TC_R1EPIC001PBI002 _01
#PBI: R1EPIC001PBI002
#User Story ID: US029, US030
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: GLCMS UID validation, CIF validation

@TC_R1EPIC001PBI002_01
Scenario: RM:Verify the field behaviors of GLCMS UID and T24 CIF ID in Advanced search screen for RM User
	
	Given I login to Fenergo Application with "RM" 
	When I navigate to "Advanced Search" screen
	When I expand the "Filters" button
#	And I check that below data is non mandatory 
#		|FieldLabel |
#		|GLCMS UID  |
#		|T24 CIF ID |
	#GLCMS UID Validation
	When I enter data in "GLCMS ID" as "ALPHABETICAL"
	Then I validate the error messgage for "GLCMS UID-Alphabetical" as "GLCMS UID can contain only digits. Only zero is invalid input."
	#Test Data: Enter values other than numeric values in GLCMS UID field
	#User should NOT be able to enter values other than numeric
	And I refresh the page
	When I expand the "Filters" button
	When I enter data in "GLCMS ID" as "NumericalMoreThenSix"
	Then I validate the error messgage for "GLCMS UID-Max Character" as "You've reached the maximum length. GLCMS ID accepts 6 characters."	
	#Test Data: Enter numeric value less than or equal to 6 digits in GLCMS UID field
	#User should be able to enter numeric values less than or equal to 6 digits
	And I refresh the page
	When I expand the "Filters" button
	When I enter data in "GLCMS ID" as "NumericalLessThenSix"
	Then I see there is no validation message
	And I refresh the page
	When I expand the "Filters" button
	When I enter data in "GLCMS ID" as "AllZeroes"
	Then I validate the error messgage for "GLCMS UID-Alphabetical" as "GLCMS UID can contain only digits. Only zero is invalid input."
	And I refresh the page
	When I expand the "Filters" button
	When I enter data in "T24 CIF ID" as "ALPHABETICAL"
	Then I validate the error messgage for "T24 CIF ID-Alphabetical" as "T24 CIF ID can contain only digits. Only zero is invalid input."
	#Test Data: Enter all zeros as 6 digit in GLCMS UID Field
	#User should NOT be able to enter only zeros in GLCMS UID field
	#T24 CIF Validation
	And I fill in data in AdvancedSearch screen with key 'Data1'
	#Test Data: Enter values other than numeric values in T24 CIF ID field
	#User should NOT be able to enter values other than numeric
	And I fill in data in AdvancedSearch screen with key 'Data2'
	#Test Data: Enter numeric value less than or equal to 10 digits in T24 CIF ID field
	#User should be able to enter numeric values less than or equal to 10 digits
	And I fill in data in AdvancedSearch screen with key 'Data3'
	#Test Data: Enter numeric value more than 10 digit in T24 CIF ID Field
	#User should NOT be able to enter numeric value more than 10 digit
	And I fill in data in AdvancedSearch screen with key 'Data4'
	#Test Data: Enter all zeros as 10 digit in T24 CIF ID Field
	#User should NOT be able to enter only zeros in T24 CIF ID field
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
