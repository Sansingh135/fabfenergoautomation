#Test Case: TC_R1EPIC001PBI002 _02
#PBI: R1EPIC001PBI002
#User Story ID: US029, US030
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed

Feature: GLCMS UID validation, CIF Validation

Scenario: RM:Verify the field behaviors of GLCMS UID and T24 CID ID in Legal Entity Search screen for RM User
	
	Given I login to Fenergo Application with "RM" 
	When I navigate to "LegalEntitySearch" screen
	And I check that below data is available 
	|FieldLabel|Mandatory |
	|GLCMS UID |NO				|
	|T24 CIF ID |NO				|
	#GLCMS UID validation
	And I fill in data in AdvancedSearch screen with key 'Data1'
	#Test Data: Enter values other than numeric values in GLCMS UID field
	#User should NOT be able to enter values other than numeric
	And I fill in data in AdvancedSearch screen with key 'Data2'
	#Test Data: Enter numeric value less than or equal to 6 digits in GLCMS UID field
	#User should be able to enter numeric values less than or equal to 6 digits
	And I fill in data in AdvancedSearch screen with key 'Data3'
	#Test Data: Enter numeric value more than 6 digit in GLCMS UID Field
	#User should NOT be able to enter numeric value more than 6 digit
	And I fill in data in AdvancedSearch screen with key 'Data4'
	#Test Data: Enter all zeros as 6 digit in GLCMS UID Field
	#User should NOT be able to enter only zeros in GLCMS UID field
	#T24 CIF ID Validation
	And I fill in data in AdvancedSearch screen with key 'Data1'
	#Test Data: Enter values other than numeric values in T24 CIF ID field
	#User should NOT be able to enter values other than numeric
	And I fill in data in AdvancedSearch screen with key 'Data2'
	#Test Data: Enter numeric value less than or equal to 10 digits in T24 CIF ID field
	#User should be able to enter numeric values less than or equal to 10 digits
	And I fill in data in AdvancedSearch screen with key 'Data3'
	#Test Data: Enter numeric value more than 10 digit in T24 CIF ID Field
	#User should NOT be able to enter numeric value more than 10 digit
	And I fill in data in AdvancedSearch screen with key 'Data4'
	#Test Data: Enter all zeros as 10 digit in T24 CIF ID Field
	#User should NOT be able to enter only zeros in T24 CIF ID field
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
