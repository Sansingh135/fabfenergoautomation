#Test Case: TC_R1EPIC01PBI001_08
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: "Accounts" subflow is displaying as hidden on "Enrich KYC Profile" task on "Enrich Client information" stage
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	When I complete "CaptureRequestDetailsGrid" task
	When I navigate to "ReviewRequest" task
	When I complete "ReviewRequest" task
	When I store the "CaseId" on LE360 screen 
	When I login to fenergo Appication with "KYCMaker"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task of "Validate data" stage
	When I complete to "ValidateKYCandRegulatoryGrid" task
	When I navigate to "EnrichClientProfileGrid" task of "EnrichKYCProfile" grid
	Then I can see "Accounts" subflow is displaying as hidden
	