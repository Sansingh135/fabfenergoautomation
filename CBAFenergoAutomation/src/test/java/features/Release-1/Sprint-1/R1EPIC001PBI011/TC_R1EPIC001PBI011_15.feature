#Test Case: TC_R1EPIC01PBI001_15
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora


Feature: COB 

Scenario: "Accounts" subflow is displaying as hidden on "Review/EditClientData" stage for Regular Review workflow
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	Then I can see "Accounts" subflow is displaying as hidden
	When I complete "CaptureRequestDetailsGrid" task	
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo application with "KYCMaker" user
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYCandRegulatoryGrid" task 
	When I navigate to "EnrichKYCProfileGrid" task 
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	When I navigate to "QualityControlGrid" task 
	When I store the "CaseId" from LE360 
	When I login to Fenergo application with "RelationshipManager" user
	When I search for "CaseID"
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task
  When I store the "CaseId" from LE360 
	When I login to Fenergo application with "FLODKYCmanager" user
	When I search for "CaseID"
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
	When I store the "CaseId" from LE360 
	When I login to Fenergo application with "FLODAVP" user
	When I search for "CaseID"
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	When I complete "FLODAVPReviewandSign-Off" task
	When I store the "CaseId" from LE360 
	When I login to Fenergo application with "BusinessUnitHead(N3)" user
	When I search for "CaseID"
	When I navigate to "BUHReviewandSignOffGrid" task 
	When I complete "BUHReviewandSignOff" task
	When I store the "CaseId" from LE360 
	When I login to Fenergo application with "FLODVP" user
	When I search for "CaseID"
	When I navigate to "FLODVPReviewandSignOffGrid" task
	When I complete "FLODVPReviewandSignOffGrid" task
	#Validating that the case status is closed
    And I assert that the CaseStatus is "Closed" 
  When I navigate to "LE360 overview" screen
  When I initiate a "regularreview" by clicking on "Actions" button  
  When I store the "CaseId" from LE360 
	When I login to Fenergo application with "KYCMaker" user
	When I search for "CaseID"
	When I navigate to "CloseAssociatedcases" task
	When I complete to "CloseAssociatedcases" task
	When I navigate to "ValidateKYCandRegulatoryData" task
	Then I can see "Accounts" subflow is displaying as hidden
	