#Test Case: TC_R1EPIC01PBI001_02
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB 

@Automation
Scenario: "Accounts" subflow is displaying as hidden on "LE360 overview-Products" screen
	Given I login to Fenergo Application with "RM"  
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I add a Product from "CaptureRequestDetails"
    Then I complete "CaptureRequestDetailsFAB" task
	When I navigate to "LE360overview" screen
#	When I view the existing Product from "CaptureRequestDetails"
	Then I can see "Accounts" subflow is hidden
	And I close the browser