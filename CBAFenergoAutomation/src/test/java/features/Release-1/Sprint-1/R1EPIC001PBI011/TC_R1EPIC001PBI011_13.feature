#Test Case: TC_R1EPIC01PBI001_13
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: <<Priyanka Arora>>
#Last Edited by: <<Priyanka Arora>>
-
Feature: COB 

Scenario: Validate "Accounts" subflow is displaying as hidden for following users on "LE360 overview" screen
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	When I complete "CaptureRequestDetailsGrid" task
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is displaying as hidden
	When I store CaseID on LE360 screen
	When I login to Fenergo application with "Onbaordingchecker"
	When I search for the "CaseID"
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is displaying as hidden
	When I store the "CaseId" on LE360 screen 
	When I login to fenergo Appication with "FLODKYCManager"
	When I search for the "CaseId" 
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is displaying as hidden
	When I login to Fenergo Application with "FLODAVP"
	When I store CaseID on LE360 screen
	When I search for the "CaseId" 
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is displaying as hidden
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "BusinessUnitHead(N3)"
	When I search for the "CaseId" 
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is displaying as hidden
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "FLODVP"
	When I search for the "CaseId" 
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is displaying as hidden
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "FLODSVP"
	When I search for the "CaseId" 
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is displaying as hidden
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "BusinessHead(N2)"
	When I search for the "CaseId" 
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is displaying as hidden
	When I store CaseID on LE360 screen
	When I login to Fenergo Application with "Compliance"
	When I search for the "CaseId" 
	When I navigate to "LE360 overview" screen
	Then I can see "Accounts" subflow is displaying as hidden