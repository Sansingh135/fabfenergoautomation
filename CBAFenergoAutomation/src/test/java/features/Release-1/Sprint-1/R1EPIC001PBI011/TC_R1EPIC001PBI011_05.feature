#Test Case: TC_R1EPIC01PBI001_05
#PBI: R1EPIC01PBI011
#User Story ID: US031
Feature: COB 

@Automation
Scenario: "Accounts" subflow is displaying as hidden on "Add Product" screen for "Capture Request details" task for "new request" stage
	Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	Then I complete "CaptureRequestDetailsFAB" task
	When I view the existing Product from "CaptureRequestDetails"
	Then I can see "Accounts" subflow is hidden
	