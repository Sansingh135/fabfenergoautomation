#Test Case: TC_R1EPIC01PBI001_07
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario:  "Accounts" subflow is displaying as hidden on "Add Product" screen for "Enrich KYC details" task on "Enrich Client information" stage
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	When I add product on "Add Product" screen of "CaptureRequestDetailsGrid" task
	When I complete "CaptureRequestDetailsGrid" task
	When I store the "CaseId" on LE360 screen 
	When I login to fenergo Appication with "KYCMaker"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task of "Validate data" stage
	When I complete to "ValidateKYCandRegulatoryGrid" task
	When I navigate to "EnrichClientProfileGrid" task of "EnrichKYCProfile" grid
	When I navigate to "Add Product" screen by clicking on "view" button
	Then I can see "Accounts" subflow is displaying as hidden
	