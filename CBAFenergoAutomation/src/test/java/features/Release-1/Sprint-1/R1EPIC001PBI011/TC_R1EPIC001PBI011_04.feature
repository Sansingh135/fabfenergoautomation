#Test Case: TC_R1EPIC01PBI001_04
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB 

@Automation
Scenario: "Accounts" subflow is displaying as hidden on "Review Request" task on "New Request" stage
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	Then I complete "CaptureRequestDetailsFAB" task
	Then I can see "Accounts" subflow is hidden