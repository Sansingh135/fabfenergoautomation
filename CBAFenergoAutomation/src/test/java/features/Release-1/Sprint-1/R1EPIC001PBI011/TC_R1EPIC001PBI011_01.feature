#Test Case: TC_R1EPIC01PBI001_01
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: US011 

Scenario: "Accounts" subflow is displaying as hidden on "LE360 overview" screen
	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	Then I complete "CaptureRequestDetailsFAB" task
	When I navigate to "LE360overview" screen
	Then I can see "Accounts" subflow is hidden
	