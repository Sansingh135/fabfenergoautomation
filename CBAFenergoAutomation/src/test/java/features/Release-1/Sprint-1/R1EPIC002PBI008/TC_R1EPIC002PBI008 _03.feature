#Test Case: TC_R1EPIC002PBI008_03
#PBI: R1EPIC002PBI008
#User Story ID: US068
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: Capture New Request Details - Complete Legal Entity Category


@Automation
Scenario: Validate the 'Legal Entity Category' dropdown is filtered with relevant values (15) in Complete screen when 'Client Type' is selected as 'FI' from the Enter entity details screen (Refer lov in the PBI)

	Given I login to Fenergo Application with "RM" 
	When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Financial Institution (FI)"
	Then I verify "Legal Entity Category" drop-down values with ClientType as "Financial Institution (FI)"
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
