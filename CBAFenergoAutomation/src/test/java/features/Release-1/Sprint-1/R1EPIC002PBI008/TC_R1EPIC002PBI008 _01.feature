#Test Case: TC_R1EPIC002PBI008_01
#PBI: R1EPIC002PBI008
#User Story ID: US068
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: Capture New Request Details - Complete Legal Entity Category 

@Sample @Automation @TC_R1EPIC002PBI008_01 
Scenario: Validate the 'Legal Entity Category' dropdown is filtered with relevant values (11) in Complete screen when 'Client Type' is selected as 'Corporate' from the Enter entity details screen (Refer lov in the PBI) 

	Given I login to Fenergo Application with "RM" 
	#Select client Type as 'Corporate' in Enter entity details screen
	#When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	#Validate the Legal Entity Category lovs (Refer lov from the PBI) in complete screen
	When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Corporate"
	Then I verify "Legal Entity Category" drop-down values with ClientType as "Corporate"
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
