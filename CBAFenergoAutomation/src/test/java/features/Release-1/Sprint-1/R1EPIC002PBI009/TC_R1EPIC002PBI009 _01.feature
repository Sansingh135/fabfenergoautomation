#Test Case: TC_R1EPIC002PBI009 _01
#PBI: R1EPIC002PBI009
#User Story ID: US055
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed

@TobeAutomated
Feature: Capture New Request Details/Review Request - Add Relationship -Relationship Type

Scenario: Validate the dropdown values of the field 'Relationship Type' in Add Relationship screen of Capture request details screen (Refer 'Relationship Type' lov in the PBI)
	
	Given I login to Fenergo Application with "RMUser" 
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	When I add relationship
	Then I validate LOVs for "Relationshiptype"
	#Refer PBI for the list
	