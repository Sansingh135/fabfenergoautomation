#Test Case: TC_R1EPIC002PBI009 _03
#PBI: R1EPIC002PBI009
#User Story ID: US055
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed

@TobeAutomated
Feature: Capture New Request Details/Review Request - Add Relationship -Relationship Type

Scenario: Verify the ability to search relationship using 'DAO code' in Search Users section of Add Relationship screen in Capture request details creen
	
	Given I login to Fenergo Application with "RMUser" 
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	When I add Relationship
	#Searching relationships using DAO Code
	#Enter valid DAO Code Ex: BNK103330737
	And I fill in data in RelationshipSearch screen
	When I click on Search button in AddRelationship screen
	#User should be able to view the relationships in the search grid corresponding to the DAO Code entered in the search criteria
	Then I validate the search grid data
	