#Test Case: TC_R1EPIC002PBI009 _07
#PBI: R1EPIC002PBI009
#User Story ID: US055
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed

Feature: Capture New Request Details/Review Request - Add Relationship -Relationship Type

Scenario: Verify the new field 'DAO code' is available under User Details section of Trading Entity, Add Relationship screen 
	
	Given I login to Fenergo Application with "RMUser" 
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	When I add trading entity
	When I add Relationship
	#Check DAO Code field is available under Search Users section
	And I check that below data is available 
	|FieldLabel |Mandatory|
	|DAO Code| No |

	

	