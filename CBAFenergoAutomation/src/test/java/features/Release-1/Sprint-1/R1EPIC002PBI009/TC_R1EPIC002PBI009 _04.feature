#Test Case: TC_R1EPIC002PBI009 _04
#PBI: R1EPIC002PBI009
#User Story ID: US055
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed

Feature: Capture New Request Details/Review Request - Add Relationship -Relationship Type

Scenario: Verify Relationship section is mandatory in Capture request details screen
	
	Given I login to Fenergo Application with "RMUser" 
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	#check the Relationship grid is mandatory

	