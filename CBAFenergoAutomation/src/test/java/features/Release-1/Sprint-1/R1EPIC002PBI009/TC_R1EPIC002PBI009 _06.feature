#Test Case: TC_R1EPIC002PBI009 _06
#PBI: R1EPIC002PBI009
#User Story ID: US055
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed

Feature: Capture New Request Details/Review Request - Add Relationship -Relationship Type

Scenario: Verify the new field 'DAO code' is NOT available under User Details section of Add Relationship screen when the user is NOT associated to RM group
	
	Given I login to Fenergo Application with "RMUser" 
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	When I add Relationship
	#Select relationship which is NOT associated to RM group
	#check the DAO Code value is blank
	And I check that below data is available 
	|FieldLabel |Value         |
	|DAO Code   |              |


	