#Test Case: TC_R1EPIC01PBI001_07
#PBI: R1EPIC01PBI001
#User Story ID: US013
#Designed by: Sanjeet Singh
#Last Edited by: Anusha PS
Feature: Products Section- Product Grid Remove Products

@Onboarding 
Scenario: To Verify RM is able to remove product from action button in "Capture Request Details" and "Review Request" screen

	Given I login to Fenergo Application with "RelationshipManager" 
	#Creating a legal entity with legal entity role as Client/Counterparty 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	And I add a Product from "CaptureRequestDetails"
	And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	When I delete the Product from "CaptureRequestDetails"
	And I assert ProductSection is empty in "CaptureRequestDetails"
	
	# Add a product again
	And I add a Product from "CaptureRequestDetails"
	And I complete "CaptureRequestDetailsFAB" task  
	Then I assert that "Remove" is not visible
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
