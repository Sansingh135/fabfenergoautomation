#Test Case: TC_R1EPIC01PBI001_05
#PBI: R1EPIC01PBI001
#User Story ID: US014
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Products Section- Grid View  

@COB 
Scenario: Validate if various users are able to see "Product Status" as a column on the "Products grid view" in "Capture Request Details", "Review Request", "ValidateKYCandRegulatory" and  "Enrich KYC Profile" screens.

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	And I navigate to "CaptureRequestDetails" task	
	#Adding product with status as "New"
	And I add a Product from "CaptureRequestDetails"
	When I complete "ReviewRequest" task 
	 Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task 
    When I complete "ValidateKYCandRegulatoryFAB" task 
	#Complete the COB flow
	 Given I login to Fenergo Application with "Onboarding Checker" 
	#Search for the caseid created by RM
	And I navigate to "CaptureRequestDetailsGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible
	And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails"
	And I navigate to "ReviewRequestGrid" task
	
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible
    And I assert "Status" column is visible in the Product Grid for "ReviewRequest"
	And I assert "Status" column is not present as a field below the grid for "ReviewRequest"
	
	Then I navigate to "ValidateKYCandRegulatoryGrid" task 
    When I expand "ProductGridExpandValidateKYC" and zoom out so that grid gets fully visible
	And I assert "Status" column is visible in the Product Grid for "ValidateKYCandRegulatory" 
	And I assert "Status" column is not present as a field below the grid for "ValidateKYCandRegulatoryFAB" 
	When I navigate to "EnrichKYCProfileGrid" task 
    When I expand "ProductGridExpandEnrichKYC" and zoom out so that grid gets fully visible
	And I assert "Status" column is visible in the Product Grid for "EnrichKYCProfile" 
	And I assert "Status" column is not present as a field below the grid for "EnrichKYCProfile"
  

	Given I login to Fenergo Application with "FLODKYCManager" 
	#Search for the caseid created by RM
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible
	And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails"
	
	And I navigate to "ReviewRequestGrid" task
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible
    And I assert "Status" column is visible in the Product Grid for "ReviewRequest"
	And I assert "Status" column is not present as a field below the grid for "ReviewRequest"
   Then I navigate to "ValidateKYCandRegulatoryGrid" task 
    When I expand "ProductGridExpandValidateKYC" and zoom out so that grid gets fully visible
	And I assert "Status" column is visible in the Product Grid for "ValidateKYCandRegulatory" 
	And I assert "Status" column is not present as a field below the grid for "ValidateKYCandRegulatoryFAB" 
	
	When I navigate to "EnrichKYCProfileGrid" task 
    When I expand "ProductGridExpandEnrichKYC" and zoom out so that grid gets fully visible
	And I assert "Status" column is visible in the Product Grid for "EnrichKYCProfile" 
	And I assert "Status" column is not present as a field below the grid for "EnrichKYCProfile"
	

	Given I login to Fenergo Application with "FLODAVP" 
	#Search for the caseid created by RM
	And I navigate to "CaptureRequestDetails" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ReviewRequest" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ValidateKYCandRegulatory" task 
  And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 

	Then I navigate to "EnrichKYCProfileSection" task
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	
	Given I login to Fenergo Application with "Business Unit Head (N3)" 
	#Search for the caseid created by RM
	And I navigate to "CaptureRequestDetails" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ReviewRequest" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ValidateKYCandRegulatory" task 
  And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	 
	Then I navigate to "EnrichKYCProfileSection" task
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Given I login to Fenergo Application with "FLOD VP" 
	#Search for the caseid created by RM
	And I navigate to "CaptureRequestDetails" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ReviewRequest" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ValidateKYCandRegulatory" task 
  And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	 
	Then I navigate to "EnrichKYCProfileSection" task
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Given I login to Fenergo Application with "FLOD SVP" 
	#Search for the caseid created by RM
	And I navigate to "CaptureRequestDetails" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ReviewRequest" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ValidateKYCandRegulatory" task 
  And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "EnrichKYCProfileSection" task
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Given I login to Fenergo Application with "Business Head (N2)" 
	#Search for the caseid created by RM
	And I navigate to "CaptureRequestDetails" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ReviewRequest" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ValidateKYCandRegulatory" task 
  And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "EnrichKYCProfileSection" task
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	
	Given I login to Fenergo Application with "Compliance" 
	#Search for the caseid created by RM
	And I navigate to "CaptureRequestDetails" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ReviewRequest" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ValidateKYCandRegulatory" task 
  And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "EnrichKYCProfileSection" task
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
		Given I login to Fenergo Application with "Fenergo - Admin" 
	#Search for the caseid created by RM
	And I navigate to "CaptureRequestDetails" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ReviewRequest" task 
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "ValidateKYCandRegulatory" task 
  And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Then I navigate to "EnrichKYCProfileSection" task
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	
	