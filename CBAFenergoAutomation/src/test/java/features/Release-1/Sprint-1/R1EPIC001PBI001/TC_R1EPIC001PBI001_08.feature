#Test Case: TC_R1EPIC01PBI001_08
#PBI: R1EPIC01PBI001
#User Story ID: US013
#Designed by: Sanjeet Singh
#Last Edited by: Anusha PS

Feature: Products Section- Product Grid Remove Products

@Onboarding 
Scenario: To Verify Onboarding Maker is able to remove product from action button in "ValidateKYC and Regulatory" and "Enrich KYC Profile" screens

	Given I login to Fenergo Application with "RelationshipManager" 
	#Creating a legal entity with legal entity role as Client/Counterparty 
	When I create new request with ClientEntityType as "FI"  and ClientEntityRole as "Client/Counterparty"
	And I complete "CaptureRequestDetails" task 
	When I add a product in "CaptureNewRequest" screen
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360
	
	Then I login to Fenergo Application with "KYCManager"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I add a product in "ValidateKYCandRegulatoryGrid" screen
	When I remove the Product from ProductGrid by clicking on "action" button
	Then I can see the product is removed and not visible in Product Grid
	
	#Add product again
	When I add a product in "ValidateKYCandRegulatoryGrid" screen
	When I complete "ValidateKYCandRegulatory" task
		 
	Then I navigate to "EnrichKYCProfileSection" task
	When I remove the Product from ProductGrid by clicking on action button
	Then I can see the product is removed and not visible in Product Grid

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
