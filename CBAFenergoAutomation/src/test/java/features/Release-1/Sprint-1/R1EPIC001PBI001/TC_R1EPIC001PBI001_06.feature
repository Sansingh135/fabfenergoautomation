#Test Case: TC_R1EPIC01PBI001_06
#PBI: R1EPIC01PBI001
#User Story ID: US014
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Products Section- Grid View  

@COB 
Scenario: Validate if various users are able to see "Product Status" as a column on the "Products grid view" in LE360 screen

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	
	
	#Adding product with status as "New"
	And I add a Product from "CaptureRequestDetails"
	When I complete "ReviewRequest" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	When I complete "ValidateKYCandRegulatory" task 
	#Complete the COB flow
	Then I store the "CaseId" from LE360
	
	Given I login to Fenergo Application with "OnboardingChecker" 
	#Search for the caseid created by RM
	When I navigate to "LE360overview" screen
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Given I login to Fenergo Application with "FLOD KYC Manager" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 

	Given I login to Fenergo Application with "FLOD AVP" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Given I login to Fenergo Application with "Business Unit Head (N3)" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Given I login to Fenergo Application with "FLOD VP" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Given I login to Fenergo Application with "FLOD SVP" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	
	Given I login to Fenergo Application with "Business Head (N2)" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
	Given I login to Fenergo Application with "Compliance" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	
		Given I login to Fenergo Application with "Fenergo - Admin" 
	#Search for the caseid created by RM
	Then I navigate to LE360 screen
	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
	And I assert "Status" column is not present as a field below the grid 
	