#Test Case: TC_R1EPIC01PBI001_02
#PBI: R1EPIC01PBI001
#User Story ID: US014
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Products Section- Grid View  

@COB 
Scenario: 1. Validate if RM is able to see "Product Status" as a column on the "Products grid view" in "Capture Request Details" screen after editing a Product for various product statuses
 and Validate the behaviour of "Products grid view" in "Capture Request Details" screen after deleting all the products. Product section should be empty

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "FI" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	And I navigate to "CaptureRequestDetails" task 
	
	#Adding product with status as "New"
	And I add a Product from "CaptureRequestDetails"
	
	#Editing product with status as "Active"
	And I edit a Product with Product Status as "Active"
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible
	And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails"

#	#Editing product with status as "Approved"
    And I edit a Product with Product Status as "Approved"
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible
    And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails"


#	And I edit a Product with Product Status as "Pending Approval"
    And I edit a Product with Product Status as "Approved"
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible
    And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails"

#	
#	#Editing product with status as "Rejected"
    And I edit a Product with Product Status as "Approved"
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible
    And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails"

#	
#	#Editing product with status as "New"
    And I edit a Product with Product Status as "New"
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible
    And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails"

	
#	#Editing product with status as "Cancelled"
    And I edit a Product with Product Status as "Cancelled"
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible
    And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails"
	#Deleting the added product
	When I delete the Product from "CaptureRequestDetails"	
#	#Validate if product section is empty
	And I assert ProductSection is empty in "CaptureRequestDetails" 