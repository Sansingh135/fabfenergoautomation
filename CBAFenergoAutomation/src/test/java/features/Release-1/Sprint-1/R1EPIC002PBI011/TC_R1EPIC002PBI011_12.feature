#Test Case: TC_R1EPIC002PBI011_12
#PBI: R1EPIC002PBI011
#User Story ID: US108
#Designed by: Priyanka Arora (as part of R1EPIC001PBI008)
#Last Edited by: Anusha PS

Feature: COB 

Scenario:Validate the behavior of "TaxIdentifierValue" field while trying to Edit/view it.
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I complete to "CaptureRequestDetailsGrid" task
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	When I complete the "ValidateKYCandRegulatoryGrid" task
	When I login to Fenergo application with "KYCmaker" user
	When I search for the "CaseId" 
	Then I navigate to "EnrichClientProfileGrid" task
	#Test Data: Tax Type: VAT ID
	When I add a "TAX Type" as "VAT ID" on "AddTaxIdentifier" screen
	When I add "TaxIdentifierValue" and click on "Save" button
	Then I can see "Tax identifier" is visible in tax identifier grid
	When I navigate to "AddTaxIdentifier" screen again by clicking on "Edit/view" button
	Then I can see "Tax identifier" and "TaxIdentifierValue" field is displaying as mandatory
	
