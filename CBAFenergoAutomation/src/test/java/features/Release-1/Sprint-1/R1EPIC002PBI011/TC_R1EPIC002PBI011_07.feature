#Test Case: TC_RR1EPIC002PBI011_07
#PBI: R1EPIC002PBI011
#User Story ID: US028
#User Story ID: US108
#Designed by: Priyanka Arora (as part of R1EPIC001PBI008)
#Last Edited by: Anusha PS
Feature: Enrich KYC profile task -Customer Details - Tax Identifier 


@Automation
Scenario: 1.Validate the behavior of "Tax Value field" for TAX identifier Type other than "VAT ID" in Capture Request Details screen
          and Validate the behavior of "Tax Value field" for TAX identifier Type as "VAT ID" and county other than "UAE" in Capture Request Details screen

	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	Then I complete "CaptureRequestDetailsFAB" task
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	When I complete "ValidateKYCandRegulatoryFAB" task
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId" 
	Then I navigate to "EnrichClientProfileGrid" task
	#Test Data: Tax Type: SSN (anything other than VAT ID)
	When I complete "TaxIdentifier" with TaxType as "SSN" and Country as "AU-Australia"
	#the behavior should be same as OOTB as below
	Then I can see "TaxIdentifierValue" is displaying as mandatory and there is no restriction for data/length 
	
	#Test Data: Tax Type: VATID, Country: Other than AE-UNITED ARAB EMIRATES
	When I add a "TAX Type" as "VATID" and Country as "United States" on "AddTaxIdentifier" screen
	#the behavior should be same as OOTB as below
	Then I can see "TaxIdentifierValue" is displaying as mandatory
	Then I can see the "TaxIdentifierValue" accepts any number of character 