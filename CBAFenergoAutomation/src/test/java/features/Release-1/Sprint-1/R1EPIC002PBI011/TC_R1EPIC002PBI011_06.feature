#Test Case: TC_R1EPIC002PBI011_06
#PBI: R1EPIC002PBI011
#User Story ID: US108
#Designed by: Priyanka Arora (as part of R1EPIC001PBI008)
#Last Edited by: Anusha PS
Feature: COB 

Scenario: Validate the behavior of "Tax Value field" for TAX identifier Type as "VAT ID" and Country as "UAE" in Enrich KYC Profile screen
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	When I complete "ValidateKYCandRegulatoryFAB" task
	Then I store the "CaseId" from LE360
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId" 
	Then I navigate to "EnrichClientProfileGrid" task
	#Test Data: Tax Type: VAT ID, Country: AE-UNITED ARAB EMIRATES
	When I complete "TaxIdentifier" with TaxType as "VAT ID" and Country as "AE-United Arab Emirates"
	Then I can see "TaxIdentifierValue" is displaying as mandatory and exactly 15 numeric characters
	Then I can see "TaxIdentifierValue" accept less than 15 numeric characters
