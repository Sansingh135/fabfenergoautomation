#Test Case: TC_R1EPIC002PBI011_03
#PBI: R1EPIC002PBI011
#User Story ID: US108
#Designed by: Priyanka Arora (as part of R1EPIC001PBI008)
#Last Edited by: Anusha PS
Feature: Enrich KYC profile task -Customer Details - Tax Identifier
 
@Automation
Scenario: Validate the behavior of "TaxIdentifierValue" field for input as "15 Alphanumeric characters/Alphabets" for TAX identifier Type as "VAT ID" and Country as "UAE" in Capture Request Details screen 

	Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsGrid" task 
	#Test Data: Tax Type: VAT ID, Country: AE-UNITED ARAB EMIRATES
	When I complete "TaxIdentifier" with TaxType as "VAT ID" and Country as "AE-United Arab Emirates" 
	When I enter TaxIdentifierValue as "Lessthan15character" 
	And I check that the "SaveandComplete" button is disabled 
	Then I assert that the warning message appears as "Tax value for VAT ID is not valid. Please enter numeric values of maximum 15 character length." 
	
	
	