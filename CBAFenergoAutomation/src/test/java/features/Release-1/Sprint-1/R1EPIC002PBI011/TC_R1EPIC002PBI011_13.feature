#Test Case: TC_R1EPIC002PBI011_13
#PBI: R1EPIC002PBI011
#User Story ID: US107
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: Enrich KYC profile task -Identifier Sub flow - Country of Tax Residency 

Scenario: Validate LOVs for "Country" field in Capture Request Details screen
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	Then I validate LOVs for "Country"
	#Refer PBI for the list
