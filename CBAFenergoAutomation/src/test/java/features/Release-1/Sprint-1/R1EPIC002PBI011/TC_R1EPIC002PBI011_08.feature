#Test Case: TC_R1EPIC002PBI011_08
#PBI: R1EPIC002PBI011
#User Story ID: US108
#Designed by: Priyanka Arora (as part of R1EPIC001PBI008)
#Last Edited by: Anusha PS

Feature: Enrich KYC profile task -Customer Details - Tax Identifier  

Scenario: Validate the behavior of "TaxIdentifierValue" field for input as "15 Alphanumeric characters/Alphabets" for TAX identifier Type as "VAT ID" and Country as "UAE" in Enrich KYC profile screen

	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I complete to "CaptureRequestDetailsGrid" task
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	When I complete the "ValidateKYCandRegulatoryGrid" task
	When I login to Fenergo application with "KYCmaker" user
	When I search for the "CaseId" 
	Then I navigate to "EnrichClientProfileGrid" task
	#Test Data: Tax Type: VAT ID, Country: AE-UNITED ARAB EMIRATES
	When I add a "TAX Type" as "VAT ID" on "AddTaxIdentifier" screen
	When I add "TaxIdentifierValue" as "15 Alphanumeric characters/Alphabets"
	Then I can see "TaxIdentifierValue" is not accepted/saved
	Then I can see informative message to enter valid taxvalue
	
	
