#Test Case: TC_R1EPIC002PBI011_14
#PBI: R1EPIC002PBI011
#User Story ID: US107
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: Enrich KYC profile task -Identifier Sub flow - Country of Tax Residency  
 

Scenario: Validate LOVs for "Country" field in Enrich KYC Profile screen
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I complete to "CaptureRequestDetailsGrid" task
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	When I complete the "ValidateKYCandRegulatoryGrid" task
	When I login to Fenergo application with "KYCmaker" user
	When I search for the "CaseId" 
	Then I navigate to "EnrichClientProfileGrid" task
	Then I validate LOVs for "Country"
	#Refer PBI for the list