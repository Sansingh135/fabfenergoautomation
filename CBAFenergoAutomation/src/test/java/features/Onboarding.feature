Feature: Onboarding

@Onboarding 
Scenario: Login and Creating an Onboarding flow for an OOTB application 

	Given I login to Fenergo Application with "SuperUser" 
	#Creating a legal entity with legal entity role as Client/Counterparty 
	When I create new request with LegalEntityrole as "Client/Counterparty"
#	And I fill the data for "CaptureNewRequest" with key "C1"
	And I complete "CaptureRequestDetails" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYCandRegulatoryFAB" task 
	Then I navigate to "EnrichKYCProfileGrid" task 
	And I check that below data is mandatory 
		|FieldLabel|
		|Date of Establishment|
		|Registration Number|
#		And I fill the data for "CaptureNewRequest" with key "C1"
		# Check that "Date of Establishment" is Mandatory 
	When I complete "EnrichKYCProfile" task 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "RiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	
	#Login with different user due to maker/checker roles
	Given I login to Fenergo Application with "SuperUser2" 
	When I search for the "CaseId" 
	When I navigate to "ReviewOnboardingGrid" task 
	When I complete "ReviewOnboarding" task
	
	
	#	Validating that the case status is closed
	And I assert that the CaseStatus is "Closed" 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
