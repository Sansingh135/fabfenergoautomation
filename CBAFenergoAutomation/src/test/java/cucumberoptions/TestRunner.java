package cucumberoptions;

import org.junit.runner.RunWith;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@RunWith(Cucumber.class)
@CucumberOptions(
		
		features="src/test/java/features/FabCOB.feature",
		glue="stepdefination",
		plugin= {"pretty","html:target/site/cucumber-pretty","json:target/cucumber/cucumber.json"},
        tags={}
		)
//extends AbstractTestNGCucumberTests 
public class TestRunner extends AbstractTestNGCucumberTests  {

     
    
	
	
}
