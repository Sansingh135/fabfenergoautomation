package stepdefination;
import java.util.HashMap;
import java.util.Map;



public class ScenarioContext {

	private Map<String,String> scenarioContext;
	public ScenarioContext() {
		
		scenarioContext=new HashMap<>();
	}
	public void setValue( String key, String value) {
        scenarioContext.put(key, value);
    }
	public Object getValue(String key){
        return scenarioContext.get(key);
    }
}
