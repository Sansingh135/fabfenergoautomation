package stepdefination;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class GenericStepDefinition extends PageBase {
	JavaScriptControls js=new JavaScriptControls();
    WebDriver driver= new ChromeDriver();	
	public static JavascriptExecutor jsExecutor;
	ScenarioContext scenariocontext=new ScenarioContext();
	
	Connection conn=null;
	CommonSteps commonsteps=new CommonSteps();
	
	public void clear_field(String fieldName) {
		js.clear(fieldName);
	}
	
	public void SelectElement(String fieldType,String label,String value) {
		
		switch(fieldType) {
		case "MultiSelectDropdown":
			MultiSelect.SelectMultipleElementsByText(label,value);
			break;
			
		case "Dropdown":
		Dropdown.SelectElementByText(label,value);
		
		case "TextBox":
			TextBox.selectElementbyText(label,value);
		
		}
	}
	
	public String getControls(String Datakey) { 
		String xpath=null;
	try {
		SqliteConnection sql=SqliteConnection.getInstance();
		ResultSet rst=sql.getControlsData("Controls",Datakey);
		if(!rst.next()) {throw new Exception("Controls information not found");}
		else if(rst.getString("identificationType").equals("XPATH")) {xpath=rst.getString("Locator");}
	   rst.close();
	}
	catch(Exception e) {
		
	}
	return xpath;

  }
	
	public void user_fill_in_datakey(String tablename,String Datakey) {
		try {
			SqliteConnection sql=SqliteConnection.getInstance();
			ResultSet rst=sql.buildQuery(tablename, Datakey);
			FillInDataWithoutValidation(rst);
			
				
			
		}
		catch(Exception e){
			assertTrue(e.getLocalizedMessage(),false);
		}
	}
	
	
	public void user_fill_in_legacy_datakey(String tablename,String Datakey) {
		try {
			SqliteConnection sql=SqliteConnection.getInstance();
			ResultSet rst=sql.buildQuery(tablename, Datakey);
			FillInDataWithoutValidation(rst);
			
				
			
		}
		catch(Exception e){
			assertTrue(e.getLocalizedMessage(),false);
		}
	}
	
	 static String getAlphaNumericString(int n) 
	    { 
	  
	        // chose a Character random from this String 
	        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	                                    + "0123456789"
	                                    + "abcdefghijklmnopqrstuvxyz"; 
	        String NumericString="0123456789";
	  
	        // create StringBuffer size of AlphaNumericString 
	        StringBuilder sb = new StringBuilder(n); 
	  
	        for (int i = 0; i < n; i++) { 
	  
	            // generate a random number between 
	            // 0 to AlphaNumericString variable length 
	            int index 
	                = (int)(AlphaNumericString.length() 
	                        * Math.random()); 
	  
	            // add Character one by one in end of sb 
	            sb.append(AlphaNumericString 
	                          .charAt(index)); 
	        } 
	  
	        return sb.toString(); 
	    } 
	 
	 static String getNumericString(int n) 
	    { 
	  
	        // chose a Character random from this String 
	       
	        String NumericString="0123456789";
	  
	        // create StringBuffer size of AlphaNumericString 
	        StringBuilder sb = new StringBuilder(n); 
	  
	        for (int i = 0; i < n; i++) { 
	  
	            // generate a random number between 
	            // 0 to AlphaNumericString variable length 
	            int index 
	                = (int)(NumericString.length() 
	                        * Math.random()); 
	  
	            // add Character one by one in end of sb 
	            sb.append(NumericString 
	                          .charAt(index)); 
	        } 
	  
	        return sb.toString(); 
	    } 
	 
	
	public String GetFenergoId() {
		String jsArg1="return window.top.location.href.toString()";
		String jsArg2="return document.getElementByClassName('header-subtitle header subtitle')(0).href";
		String FenergoId="";
		for (int i=1;i<5;i++) {
			String title=js.performAndReturn(driver,"return document.title").toString();
			if(title.equals("LE Details")) {
				FenergoId=js.performJsAndReturn(driver,jsArg1).split("app/")[1].split("/")[0].toString();}
			else {
				FenergoId=js.performJsAndReturn(driver,jsArg2).split("app/")[1].split("/")[0].toString();
				
			}
				if(FenergoId!=null)
				{
					System.out.println("Fenergo Id is :" +FenergoId);
					break;}
				
				
				
			}
			if(FenergoId==null) {
				System.out.println("kajsksda");
			}
			return FenergoId;
			
		}
	
	
	public void FileUpload(String xpath, String filename)
	
	{
	FileUpload.fileuploadPage(commonsteps.getXpathFromControls(xpath),filename);
	    //js.waitForPageLoad();
	}
	
	
	public void clickgridviajs(String columnName) {
		try 
		{
			
			jsExecutor.executeScript("PageObjects.find({gridColumnName:'" +columnName+"'}).select()");
			Thread.sleep(200);
		}
		catch(Exception e) {
			assertTrue(e.getLocalizedMessage(),false);
		}
		
	}
	
	public void user_Select_Dropdown(String value,String dataKey) {
		try {
			
	SqliteConnection sql=new SqliteConnection();
	dataKey=commonsteps.InputParameter(dataKey);
	ResultSet rst=sql.getControlsData("Controls", dataKey);
	if(!rst.next()) {
		ResultSet rst2=sql.getControlsData("FieldData", dataKey);
		if(!rst2.next()) {
			throw new Exception("Controls not found for Datakey" +dataKey);
		}
		else {
			Dropdown.SelectElementByText(rst2.getString("Label"),value,rst.getString("Datakey"));}
		rst2.close();
	}
	else if(rst.getString("IdentificationType")!=null) {
		if(rst.getString("IdentificationType").equals("XPATH")) {
			Dropdown.selectDropdownvalue(rst.getString("locator"),value);
		}
		else {
			Dropdown.SelectElementByText(rst.getString("Label"),value);
		}
		rst.close();
		
	}
		}
		catch(Exception e) {
			assertTrue(e.getLocalizedMessage(),false);
		}
	}
	 public void enter_value_in_testbox_using_label(String value,String label,String dataKey) {
		 String expectedvalue =commonsteps.InputParameter(value);
		 TextBox.fillInTextBox(label,expectedvalue,dataKey);
		 
	 }
	 
	 public void store_caseId(String key,String xpathkey) {
		 if(key.equalsIgnoreCase("FenergoId")) {
			 
			 scenariocontext.setValue(key,GetFenergoId());
		 }
		 
		 String xpath=commonsteps.getXpathFromControls(xpathkey);
		 try 
		 {String value=TextBox.getTextvaluelegacyPage(xpath);
		 value=value.contains("ID:")?value.replace("ID:",""):value;
		 scenariocontext.setValue(key,value);
		 System.out.println(key +":"+ value);
		 
		 }
		 catch(Exception e) {
			 assertTrue(e.getLocalizedMessage(),false);
		 }
	 }
	
	public void user_navigate_topage(String screenname,String xpath) {
		driver.findElement(By.xpath(commonsteps.getXpathFromControls(xpath))).click();;
	}
	
	
	public void user_navigate_to_le360() {
		
	}
	
	
	}


