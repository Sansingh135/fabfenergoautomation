package stepdefination;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;


public class CommonSteps {
	
	ScenarioContext scenariocontext=new ScenarioContext();
	
	public String InputParameter(String data) {
		String cellValue=null;
		if(data!=null) {
			if(data.contains("[")) {
				//String scdata=getValueMatchingRegrex(data,"?<=\\).+");
				String scdata=null;
				if(scenariocontext.getValue(scdata)!=null) {
					
					cellValue=data.replaceAll("?<=\\).+",(String)scenariocontext.getValue(scdata));
					}
				else {
					cellValue=data;
				}
			}
			
			
			
		}
		return cellValue;
		}
	
	
	public ArrayList<String> parseListData(String data,String value) throws Exception{
		ArrayList<String> dropdown=new ArrayList<>();
		JSONArray jsnarray = new JSONArray(data);
//		System.out.println("Length is" +jsnarray.length());
		for(int i=0;i<jsnarray.length();i++) {
			JSONObject jsonobj=jsnarray.getJSONObject(i);
			String actVal=jsonobj.get(value).toString().trim();
			
			dropdown.add(actVal);
//			System.out.println("Value is"+actVal);
			
		}
			
		
		
		
		return dropdown;
	}
	public String getXpathFromControls(String dataKey) {
		
		
		String xpath=null;
		try {
			SqliteConnection sql=new SqliteConnection();
			ResultSet rst=sql.getControlsData("Controls", dataKey);
			if(rst.getString("identifierType")!=null) {
                    if(rst.getString("Identifier").equals("XPATH")) {
                    	xpath=rst.getString("locator");
                    }	
                    
                    else 
                    	throw new Exception("xpath for key "+dataKey+ "not found");
			}
			
			rst.close();
			
		}
		catch(Exception e) {
			assertTrue(e.getLocalizedMessage(),false);
		}
		
		
		return xpath;
	}
	
	public static String getTextfromFile(String filepath) throws IOException{
		File file=new File(filepath);
		String absoluteFilepath=file.getAbsolutePath();
		byte[] encoded=Files.readAllBytes(Paths.get(absoluteFilepath));
		return new String(encoded,Charset.defaultCharset());
		}
	
	

}
