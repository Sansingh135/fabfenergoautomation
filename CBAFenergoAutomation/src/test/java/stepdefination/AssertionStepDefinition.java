package stepdefination;

import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;

public class AssertionStepDefinition {

	JavaScriptControls js = new JavaScriptControls();
	CommonSteps commonsteps = new CommonSteps();

	public void assert_fields(String dataKey, String expected) {
		Element element = new Element();
		try {
			SqliteConnection sql = SqliteConnection.getInstance();
			dataKey = commonsteps.InputParameter(dataKey);
			ResultSet rst = sql.getControlsData("Controls", dataKey);
			if (!rst.next()) {
				throw new Exception("Control Information not found for datakey :" + dataKey);
			}

			else if (rst.getString("IdentificationType") != null) {
				if (rst.getString("IdentificationType").equals("XPATH")) {
					String xpath = rst.getString("Locator");

					switch (expected) {

					case "disabled":
						assertTrue("Field '" + dataKey + "' is not disabled", !element.getElement(xpath).isEnabled());
						break;

					case "Enabled":
						assertTrue("Field '" + dataKey + "' is not enabled",
								element.getElement(rst.getString("Locator").toString()).isEnabled());
						break;
					case "mandatory":
						assertTrue("Field '" + dataKey + "' is not enabled",
								element.getElement(rst.getString("Locator").toString()).isDisplayed());
					}
				}

			} else {
				String fieldName = rst.getString("Label");
				switch (expected) {
				case "disabled":
					assertTrue("Field '" + dataKey + "'is not disabled", !js.IsEnabled(fieldName));
					break;
				case "enabled":
					assertTrue("Field '" + dataKey + "'is not disabled", js.IsDisabled(fieldName));
					break;
				case "mandatory":
					assertTrue("Field '" + dataKey + "'is not disabled", js.IsMandatory(fieldName));

				}
				rst.close();

			}

		} catch (Exception e) {

		}

	}

}