package stepdefination;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.ITestResult;

import java.sql.ResultSet;


public class PageBase  {
public static JavascriptExecutor jsExecutor;
	
ScenarioContext scenarioContext=new ScenarioContext();	
static JavaScriptControls js=new JavaScriptControls();
	
	

	
	public void FillInDataWithoutValidation(ResultSet dataSet) {
	
	try {
		
		while (dataSet.next()) {
			String value=dataSet.getString("FieldValue");
			String labelName=dataSet.getString("label");
			String fieldName=dataSet.getString("FieldName");
			String identificationType=dataSet.getString("identificationType");
			String locator=dataSet.getString("locator");
			String type=dataSet.getString("type");
			if (value.contains("{1}")){
				value=value.replace("{1}","_");
				scenarioContext.setValue(fieldName,value);
				if(identificationType!=null && identificationType.equals("XPATH")) {
					FillInDataWithLocators(type,locator,value,labelName);
				   switch(dataSet.getString("type")) {
					
					case "TextBox":
						TextBox.fillInTextBox(labelName,value,fieldName);
					case "TextArea":
						TextArea.fillInTextArea(labelName,value,fieldName);
					case "Dropdown":
						Dropdown.SelectElementByText(labelName,value,fieldName);
					case "CheckBox":
						CheckBox.FillInCheckBox(labelName,Boolean.valueOf(value));
						
					}
				}
			}
			
			
		}
		
		
		
	}
	catch(Exception e) {
		assertTrue(e.getLocalizedMessage(),false);
	}
}

	
	public void FillInDataWithLocators(String type,String xpath,String value, String labelName) {
		
		try {
			switch(type) {
			
			case "Textbox":
				TextBox.fillInLegacyPageTextBox(xpath,value);
				break;
			case "TextArea":
				TextArea.fillInLegacyPageTextArea(xpath,value);
			
	    }
				
				
		}
		catch(Exception e) {
			
		}
	

}
	public void FillInDataOnLegacyPage(ResultSet dataSet) {
		try {
			
			while(dataSet.next()) {
				if(dataSet.getString("FieldValue")==null||dataSet.getString("FieldValue").equals("")||dataSet.getString("FieldName").equals("Key")) {
					System.out.println(dataSet.getString("FieldName"));
					String xpath=dataSet.getString("path");
					String value=dataSet.getString("FieldValue");
					
					switch(dataSet.getString("type")) {
					
					case "TextBox":
						TextBox.fillInLegacyPageTextBox(xpath,value);
					case "Dropdown":
						Dropdown.fillInLegacyPageDropdown(xpath,value);
						 	
					}
				}
				
			}
		}
		
		catch(Exception e) {
			assertTrue(e.getLocalizedMessage(),false);
			
		}
	}
	

	
}